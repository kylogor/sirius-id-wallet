import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Recovery3Page } from './recovery3.page';

xdescribe('Recovery3Page', () => {
  let component: Recovery3Page;
  let fixture: ComponentFixture<Recovery3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Recovery3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Recovery3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
