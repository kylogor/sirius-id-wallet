import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import {Router} from '@angular/router';
import { ContactService } from 'src/service/contact.service';
import { AlertController } from '@ionic/angular';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  errorScan = false;
  constructor(
    private barcodeScanner: BarcodeScanner,
    protected deeplinks: Deeplinks,
    private router: Router,
    public contact: ContactService,
    public alertController: AlertController
    ) { 
    }

  ngOnInit() {
  }

  async deeplinkReceived(data){
    console.log("cho nay ne");
    if (data){
      if (data.length == 40){
        this.contact.dataReceived = data;
          this.router.navigate(['/contact-received']);
      }
    }
  }

  async scanCode(){
    this.barcodeScanner.scan().then(barcodeData => {
      if (barcodeData.text.length == 40){
        this.contact.dataReceived = barcodeData.text;
        this.router.navigate(['/contact-received']);
      }
      else if(barcodeData.text.length > 0){
        this.presentAlert();
      }
    }, error => {
      console.log(error);
    });
  }

  contactDetail(index:number){
    this.contact.contactDetailIndex = index;
    this.router.navigate(['/tabs/contact/contact-detail']);
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error!',
      message: '<h3>Wrong Address</h3><p>Scan again</p>',
      buttons: [
        {
          text: 'Cancel' ,
          cssClass: 'secondary',
          handler: () => {
            
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  async ionViewWillEnter(){
    this.contact.observableIsOnline.subscribe(messagge => {
      this.deeplinkReceived(this.contact.contactReceived).then(() => {
        this.contact.contactReceived = null;
      });
    });
  }
}
