import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdditionRequiresPage } from './addition-requires.page';

describe('AdditionRequiresPage', () => {
  let component: AdditionRequiresPage;
  let fixture: ComponentFixture<AdditionRequiresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionRequiresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdditionRequiresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
