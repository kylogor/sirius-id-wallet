import { Component, OnInit, ViewChild } from '@angular/core';
import { DataForConfirmService } from 'src/service/data-for-confirm.service';
import { ActionSheetController, Platform } from '@ionic/angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { IonContent } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Account } from 'tsjs-xpx-chain-sdk';
import { IpfsConnection, BlockchainNetworkConnection, ConnectionConfig, Uploader, UploadParameter, Protocol, Uint8ArrayParameterData } from 'tsjs-chain-xipfs-sdk';
import { StoredAccountService } from 'src/service/stored-account.service';
import { ApiNodeService } from 'src/service/api-node.service';
import { CredentialStored, ApiNode, AdditionStored } from 'siriusid-sdk';
import { Router } from '@angular/router';
import { Base64 } from '@ionic-native/base64/ngx';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-addition-requires',
  templateUrl: './addition-requires.page.html',
  styleUrls: ['./addition-requires.page.scss'],
})
export class AdditionRequiresPage implements OnInit {

  @ViewChild(IonContent,{static:false}) ionContent: IonContent;

  flag:boolean = false;
  image:any[] = new Array();
  index: number = 0;
  additionRequires;
  loading = false;
  constructor(
    private dataForConfirmService: DataForConfirmService,
    private actionSheetController: ActionSheetController, 
    private platform: Platform, 
    private camera: Camera, 
    private file: File, 
    private webView: WebView,
    private filePath: FilePath,
    private router: Router,
    private base64: Base64,
    private sanitize: DomSanitizer,
  ) { 
    this.additionRequires = this.dataForConfirmService.messageReceived.message.payload.addition;
  }

  ngOnInit() {
  }

  async uploadImage(index) {
    const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, index);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.CAMERA,index);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType, index:number) {
    this.flag = true;
    var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        targetHeight:500,
        targetWidth:500,
        saveToPhotoAlbum: false,
        correctOrientation: true,
    };
 
    // this.camera.getPicture(options).then(async imagePath => {

    //   this.base64.encodeFile(imagePath).then((base64File: string) => {
    //     console.log("hinh thu: " + index);
    //     console.log(base64File);
    //     console.log(base64File.length);
    //     if (this.platform.is('android')){
    //       if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
    //         this.filePath.resolveNativePath(imagePath).then(filePath => {
    //             let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //             let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
    //             this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),index,base64File);
    //         });
    //       } 
    //       else {
    //         var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //         var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //         this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),index,base64File);
    //       }
    //     }
    //     else if (this.platform.is('ios')){
    //       var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //       var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //       this.copyFileToLocalDirOnIos(correctPath, currentName, this.createFileName(),index,base64File);
    //     }
    //     this.flag = false;
    //   }, (err) => {
    //     console.log(err);
    //   });
    // });

    this.camera.getPicture(options).then(async imagePath => {
      if (this.platform.is('android')){
        this.base64.encodeFile(imagePath).then((base64File: string) => {
          if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath).then(filePath => {
                let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),index,base64File);
            });
          } 
          else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),index,base64File);
          }
        }, (err) => {
          console.log(err);
        });
      }
      else if (this.platform.is('ios')){
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDirOnIos(correctPath, currentName, this.createFileName(),index);
      }
      this.flag = false;
    });
  }

  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName, index, content) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.image[index] = {
        path:this.webView.convertFileSrc(this.file.dataDirectory + newFileName),
        content: content
      }
    }, error => {
        console.log('Error while storing file.');
    });
  }

  copyFileToLocalDirOnIos(namePath, currentName, newFileName, index) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      // this.avatar = this.storedInfo.avatar;
      const win: any = window;
      this.base64.encodeFile(win.Ionic.WebView.convertFileSrc(this.file.dataDirectory + newFileName)).then((base64File: string) => {
        this.image[index] = {
          path: this.sanitize.bypassSecurityTrustResourceUrl(win.Ionic.WebView.convertFileSrc(this.file.dataDirectory + newFileName)),
          content: base64File
        }
      }, (err) => {
        console.log(err);
      });
      
      // this.storedInfo.setAvatar(avatar);
    }, error => {
        console.log('Error while storing file.');
    });
  }

  scrollContent() {
    this.ionContent.scrollToTop(300);
  }

  async next(){
    console.log("next btn");
    this.scrollContent();
    this.loading = true;
    console.log("loading --->" + this.loading);
    let listAddition = new Array();
    for(let i=0;i<this.image.length;i++){
      // let addition = {
      //   type: this.additionRequires[i].type,
      //   name: this.additionRequires[i].name,
      //   description: this.additionRequires[i].description,
      //   content: this.image[i].content
      // } 
      listAddition[i] = await this.createKeyPair(this.image[i].content,this.additionRequires[i].name,this.additionRequires[i].description,this.additionRequires[i].type);
    }
    this.dataForConfirmService.messageReceived.message.payload.addition = listAddition;
    console.log(listAddition);
    this.loading = false;
    this.router.navigate(['/tabs/main/confirm']);
  }

  async createKeyPair(content,name,description,type){
    console.log("vao day 111");
    const account = Account.generateNewAccount(CredentialStored.NETWORK);
    const pubKey = account.publicKey;
    const priKey = account.privateKey;
    const additionHash = await this.storedAddition(content,name,description,type,pubKey,priKey);
    console.log("pubKey: " + pubKey + "---" + "priKey: " + priKey + + "---" + "additionHash: " + additionHash);
    return new AdditionStored(pubKey,priKey,additionHash);
  }
  async storedAddition(uri: string,name:string,description:string,type:string, publicKey:string, privateKey:string){
    const data = this.convertDataURIToBinary(uri);
    const sender = Account.createFromPrivateKey(StoredAccountService.getPrivateKey(),ApiNodeService.NETWORK_TYPE);
    const ipfsConnection = new IpfsConnection(
        CredentialStored.ipfsDomain, // the host or multi address
        CredentialStored.ipfsPort, // the port number
        { protocol: CredentialStored.ipfsProtocol } // the optional protocol
    );

    // Creates Proximax blockchain network connection
    console.log(ApiNode.apiNode);
    const apiHost = this.convertApiNode();
    const blockchainConnection = new BlockchainNetworkConnection(
      CredentialStored.convertNetworkType(), // the network type
        apiHost.apiDomain, // the rest api base endpoint
        apiHost.apiPort, // the optional websocket end point
        apiHost.apiProtocol
    );

    // Connection Config
    const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);

    const uploader = new Uploader(conectionConfig);
    const metadata = new Map<string,string>([
      ['name', name],
      ['description', description],
      ['type', type],
    ]) 
    const param = Uint8ArrayParameterData.create(data,null,null,null,metadata);

    const uploadParamBuilder = UploadParameter.createForUint8ArrayUpload(param,StoredAccountService.getPrivateKey())
    const uploadParams = uploadParamBuilder
      .withRecipientPublicKey(sender.publicKey)
      .withTransactionMosaics([])
      .withNemKeysPrivacy(publicKey,privateKey)
      .build();
    return new Promise(resolve => {
        uploader.upload(uploadParams).then(result => {
            console.log("transaction hash: " + result.transactionHash);
            resolve(result.transactionHash);
        });
    })
  }

  convertApiNode(){
    let url;
    console.log();
    const lastCharacter = ApiNodeService.apiNode[ApiNodeService.apiNode.length-1];
    if (lastCharacter == "/"){
        url = ApiNodeService.apiNode.slice(0,ApiNodeService.apiNode.length-1);
    }
    else url = ApiNodeService.apiNode;

    const httpsIs = url.indexOf('https');
    let apiProtocol;
    let apiPort;
    let apiDomain;
    if (httpsIs == -1){
        apiProtocol = Protocol.HTTP;
        if (url.indexOf(':') > -1){
            apiDomain = url.slice(7,url.indexOf(':'));
            apiPort = Number(url.slice(url.indexOf(':')+1));
        }
        else {
            apiDomain = url.slice(7);
            apiPort = 80;
        }
    }
    else {
        apiProtocol = Protocol.HTTPS;
        apiPort = 443;
        apiDomain = url.slice(8);
    }
    console.log("=====================");
    console.log(apiProtocol + " " + apiDomain + " " + apiPort);
    console.log("=====================");
    return {
        'apiDomain' : apiDomain,
        'apiPort' : apiPort,
        'apiProtocol' : apiProtocol
    }
  }


  convertDataURIToBinary(dataURI: string) {
    const BASE64_MARKER = ';base64,';
    const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    const base64 = dataURI.substring(base64Index);
    const raw = window.atob(base64);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
  }
}
