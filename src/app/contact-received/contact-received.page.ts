import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ContactService } from 'src/service/contact.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-contact-received',
  templateUrl: './contact-received.page.html',
  styleUrls: ['./contact-received.page.scss'],
})
export class ContactReceivedPage implements OnInit {

  constructor(
    public alertController: AlertController,
    public contact: ContactService,
    private router: Router
  ) { 
    console.log(this.contact.dataReceived);
  }

  async addContact() {
    const alert = await this.alertController.create({
      header: 'Type Name',
      inputs: [
        {
          name: 'name',
          type:'text',
          placeholder: 'Type name'
        }
      ],
      buttons: [
        {
          text: 'Cancel' ,
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Save' ,
          cssClass: '',
          handler: (received) => {
            this.contact.addContact(received.name);
            this.router.navigate(['/tabs/contact/']);
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }
  ngOnInit() {
  }

}
