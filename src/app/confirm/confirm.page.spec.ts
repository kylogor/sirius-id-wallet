import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AuthGuardService} from '../../service/auth-guard.service';
import { ConfirmPage } from './confirm.page';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('ConfirmPage', () => {
  let component: ConfirmPage;
  let fixture: ComponentFixture<ConfirmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthGuardService,
        { provide: Router, useClass: MockRouter }
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
