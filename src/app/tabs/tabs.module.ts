import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { AuthGuardConfirmService } from '../../service/auth-guard-confirm.service';
import { AuthGuardConfirm1Service } from '../../service/auth-guard-confirm1.service';

import { AuthGuardCredentialDetailService } from '../../service/auth-guard-credential-detail.service';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:
      [
        {
          path: 'main',
          children: [
            {
              path: '',
              loadChildren: '../main/main.module#MainPageModule'
            },
            {
              path: 'confirm',
              loadChildren: '../confirm/confirm.module#ConfirmPageModule',
              canActivate: [AuthGuardConfirmService]
            },
            {
              path: 'confirm1',
              loadChildren: '../confirm1/confirm1.module#Confirm1PageModule',
              canActivate: [AuthGuardConfirm1Service]
            },
            {
              path: 'history',
              loadChildren: '../history/history.module#HistoryPageModule'
            },
            {
              path: 'transactionDetail',
              loadChildren: '../transaction-detail/transaction-detail.module#TransactionDetailPageModule'
            },
            {
              path: 'transactionDetail1',
              loadChildren: '../transaction-detail1/transaction-detail1.module#TransactionDetail1PageModule'
            },
            {
              path: 'confirm-error',
              loadChildren: '../confirm-error/confirm-error.module#ConfirmErrorPageModule'
            },
            {
              path: 'credential-detail',
              loadChildren: '../credential-detail/credential-detail.module#CredentialDetailPageModule',
              canActivate: [AuthGuardCredentialDetailService]
            },
            {
              path: 'folder-content/:name',
              loadChildren: '../folder-content/folder-content.module#FolderContentPageModule'
            },
            {
              path: 'credential-group-detail',
              loadChildren: '../credential-group-detail/credential-group-detail.module#CredentialGroupDetailPageModule'
            },
            {
              path: 'addition-requires',
              loadChildren: '../addition-requires/addition-requires.module#AdditionRequiresPageModule'
            },
          ]
        },
        {
          path: 'contact',
          children: [
            { path: '', loadChildren: '../contact/contact.module#ContactPageModule' },
            { path: 'contact-detail', loadChildren: '../contact-detail/contact-detail.module#ContactDetailPageModule' },

          ]
        },
        {
          path: 'user',
          children: [
            { path: '', loadChildren: '../user/user.module#UserPageModule' },
            { path: 'qr', loadChildren: '../my-qrcode/my-qrcode.module#MyQRcodePageModule' },
            { path: 'user1', loadChildren: '../user1/user1.module#User1PageModule' },
          ]
        },
        {
          path: 'notification',
          children: [
            { path: '', loadChildren: '../notifications/notifications.module#NotificationsPageModule' },
          ]
        },
        {
          path: 'setting',
          children: [
            { path: '', loadChildren: '../setting/setting.module#SettingPageModule' },
            { path: 'nortification', loadChildren: '../nortification/nortification.module#NortificationPageModule' },
            { path: 'backup', loadChildren: '../backup/backup.module#BackupPageModule' },
            { path: 'node-setting', loadChildren: '../node-setting/node-setting.module#NodeSettingPageModule' },
            { path: 'sirius-did', loadChildren: '../sirius-did/sirius-did.module#SiriusDidPageModule' },
          ]
        },
        {
          path: '',
          redirectTo: '/tabs/main',
          pathMatch: 'full'
        }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
