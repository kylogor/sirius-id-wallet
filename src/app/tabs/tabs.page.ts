import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { AuthGuardService } from '../../service/auth-guard.service';
import { NotificationService } from '../../service/notification.service';
import { StoredInfoService } from '../../service/stored-info.service';
import { StoredAccountService } from '../../service/stored-account.service';
import { ApiNodeService } from '../../service/api-node.service';
import { CredentialsService } from '../../service/credentials.service';
@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  @ViewChild('myTabs', { static: false }) tabs: IonTabs;

  homeTab = true;
  contactTab = false;
  userTab = false;
  settingTab = false;
  notificationTab = false;

  constructor(
    // private authGuardService: AuthGuardService,
    public notificationService: NotificationService,
    private storedInfo: StoredInfoService,
    private storedAccount: StoredAccountService,
    private apiNodeService: ApiNodeService,
    private credentialService: CredentialsService
  ) {
    // this.authGuardService.auth = false;
  }

  ngOnInit() {
  }

  getSelectedTab() {
    if (this.tabs.getSelected() === 'main') {
      this.homeTab = true;
      this.contactTab = false;
      this.userTab = false;
      this.settingTab = false;
      this.notificationTab = false;
    } else if (this.tabs.getSelected() === 'contact') {
      this.homeTab = false;
      this.contactTab = true;
      this.userTab = false;
      this.settingTab = false;
      this.notificationTab = false;
    } else if (this.tabs.getSelected() === 'user') {
      this.homeTab = false;
      this.contactTab = false;
      this.userTab = true;
      this.settingTab = false;
      this.notificationTab = false;
    } else if (this.tabs.getSelected() === 'notification') {
      this.notificationTab = true;
      this.homeTab = false;
      this.contactTab = false;
      this.userTab = false;
      this.settingTab = false;
    } else if (this.tabs.getSelected() === 'setting') {
      this.homeTab = false;
      this.contactTab = false;
      this.userTab = false;
      this.notificationTab = false;
      this.settingTab = true;
    }
  }
}
