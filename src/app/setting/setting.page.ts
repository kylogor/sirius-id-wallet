import { Component, OnInit } from '@angular/core';
import {AlertController} from '@ionic/angular';
var pjson = require('../../../package.json');

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  constructor(
    public alertController: AlertController
  ) {}

  versionNumber: string = pjson.version;

  ngOnInit() {}

  async version(){
    const alert = await this.alertController.create({
      header: 'SiriusID version number',
      message: this.versionNumber,
    });
    await alert.present();
  }

}
