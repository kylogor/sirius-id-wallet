import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CredentialGroupDetailPage } from './credential-group-detail.page';

describe('CredentialGroupDetailPage', () => {
  let component: CredentialGroupDetailPage;
  let fixture: ComponentFixture<CredentialGroupDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialGroupDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CredentialGroupDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
