import { Component, OnInit } from '@angular/core';
import { CredentialsService } from '../../service/credentials.service';
import { AuthGuardCredentialDetailService } from 'src/service/auth-guard-credential-detail.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-credential-group-detail',
  templateUrl: './credential-group-detail.page.html',
  styleUrls: ['./credential-group-detail.page.scss'],
})
export class CredentialGroupDetailPage implements OnInit {

  group;
  constructor(
    private credentialsService: CredentialsService,
    private authDetail: AuthGuardCredentialDetailService,
    private router: Router
  ) { 
    this.group = this.credentialsService.credentialGroup[this.credentialsService.credentialGroupIndex];
  }

  ngOnInit() {
  }

  /**
   * @param {number} index
   * @memberof MainPage
   */
  credentialDetail(index: number) {
    console.log("vao day");
    console.log(index);
    console.log(this.group);
    console.log(this.credentialsService.credentialGroupIndex);
    console.log(this.group['list'][index]['id']);
    console.log(this.credentialsService.credentials.length);
    for (let i=0;i<this.credentialsService.credentials.length;i++){
      if (this.group['list'][index]['content']['id'] == this.credentialsService.credentials[i]['content']['id']){
        this.credentialsService.credentialDetailIndex = i;
        console.log(i);
        break;
      }
    }
    console.log("toi day");
    this.authDetail.auth = true;
    this.router.navigate(['/tabs/main/credential-detail']);
  }
}
