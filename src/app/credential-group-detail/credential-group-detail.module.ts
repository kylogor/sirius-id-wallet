import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CredentialGroupDetailPageRoutingModule } from './credential-group-detail-routing.module';

import { CredentialGroupDetailPage } from './credential-group-detail.page';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CredentialGroupDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [CredentialGroupDetailPage]
})
export class CredentialGroupDetailPageModule {}
