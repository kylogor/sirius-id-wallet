import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CredentialGroupDetailPage } from './credential-group-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CredentialGroupDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CredentialGroupDetailPageRoutingModule {}
