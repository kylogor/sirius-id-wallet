import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../../service/history.service';
@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.page.html',
  styleUrls: ['./transaction-detail.page.scss'],
})
export class TransactionDetailPage implements OnInit {

  type: any;
  recepient: any;
  signer: any;
  dateTime: any;
  i: any;
  constructor(private historyService: HistoryService) { 
    this.i = this.historyService.i;
    this.recepient = this.historyService.recepient[this.i];
    this.signer = this.historyService.signer[this.i];
    this.dateTime = this.historyService.dateTime[this.i];
    this.type = this.historyService.type[this.i];
  }

  ngOnInit() {
  }

}
