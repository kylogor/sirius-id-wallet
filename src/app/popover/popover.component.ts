import { Component, OnInit } from '@angular/core';
import { FoldersService } from 'src/service/folders.service';
import { PopoverController } from '@ionic/angular';
import {  } from "module";


@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  addFolderActive = false
  folderName = ''

  constructor(
    public folderService: FoldersService,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {
    console.log('STORAGE FOLDERS');
    this.folderService.getStorageFolders()
  }

  changeFoldersStatus () {
    this.addFolderActive = true
  }

  createNewFolder() {
    if (this.folderName === '') {
      this.folderName = `NewFolder${this.folderService.folders.length + 1}`
      this.folderService.newFolder(this.folderName)
    } else {
      this.folderService.newFolder(this.folderName)
    }

    this.popoverController.dismiss()
  }

  dismissPop() {
    this.popoverController.dismiss()
  }

}
