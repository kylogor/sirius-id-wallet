import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Recovery2Page } from './recovery2.page';
import {AuthGuardConfirmService} from '../../service/auth-guard-confirm.service' ;
import {AuthGuardService} from '../../service/auth-guard.service';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('Recovery2Page', () => {
  let component: Recovery2Page;
  let fixture: ComponentFixture<Recovery2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Recovery2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthGuardConfirmService,
        AuthGuardService,
        { provide: Router, useClass: MockRouter }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Recovery2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
