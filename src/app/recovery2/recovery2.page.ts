import { Component, OnInit } from '@angular/core';
import {AuthGuardService} from '../../service/auth-guard.service';

@Component({
  selector: 'app-recovery2',
  templateUrl: './recovery2.page.html',
  styleUrls: ['./recovery2.page.scss'],
})
export class Recovery2Page implements OnInit {

  constructor(
    // private authGuardService: AuthGuardService
  ) { }

  ngOnInit() {
  }

  ionViewDidLeave(){
    // this.authGuardService.auth = false;
  }
}
