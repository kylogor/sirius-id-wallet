import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { DeeplinkDataService } from 'src/service/deeplink-data.service';
import { ContactService } from 'src/service/contact.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private deeplink: Deeplinks,
    private deeplinkData: DeeplinkDataService,
    private contact: ContactService,
    private router: Router
  ) {
    console.log('=========== INIT APP ===========');
    this.initializeApp();
    this.splashScreen.show();
    setTimeout(() => {
      this.splashScreen.hide();
    }, 2000);
  }

  /**
   *
   *
   * @memberof AppComponent
   */
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.deeplink.route({
        '/app/contact/:address': {},
        '/app/data/:data': {},
      }).subscribe((match) => {
        console.log('deeplink match');
        console.log(match);
        if (match.$link.path.indexOf('/app/data/') > -1) { // Route is Data
          console.log('Route is Data');
          this.deeplinkData.messaggeReceived = match.$args.data;
          this.router.navigate(['/home']);
          this.deeplinkData.observableIsOnline.next(this.deeplinkData.messaggeReceived);
        } else if (match.$link.path.indexOf('/app/contact/') > -1) { // Route is Contact
          console.log('Route is Contact');
          this.contact.contactReceived = match.$args.address;
          this.router.navigate(['/tabs/contact']);
          this.contact.observableIsOnline.next(this.contact.contactReceived);
        }
      }, (nomatch) => {
        console.log('\n===== Deeplink not match =====', nomatch);
      });
    });
  }
}
