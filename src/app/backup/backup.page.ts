import { Component, OnInit } from '@angular/core';
import { StoredAccountService } from '../../service/stored-account.service';
import { Bip39 } from 'siriusid-sdk';
import {Clipboard} from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-backup',
  templateUrl: './backup.page.html',
  styleUrls: ['./backup.page.scss'],
})
export class BackupPage implements OnInit {

  aggree = false;
  show = false;
  passPhrase:string = null;
  copy:boolean = false;

  constructor(private cliboard: Clipboard) {
    let privateKey = StoredAccountService.getPrivateKey();
    this.passPhrase = Bip39.EntropyToMnemonic(privateKey);
  }


  showPassPhrase(){
    if (this.aggree){
      this.show = true;
    }
  }

  hidePassPhrase() {
    this.show = false;
    this.aggree = false;
  }

  ngOnInit() {
  }

  ionViewDidLeave(){
    this.aggree = false;
    this.show = false;
    this.copy = false;
  }

  copyText(){
    this.cliboard.copy(this.passPhrase);
    this.copy = true;
  }

}
