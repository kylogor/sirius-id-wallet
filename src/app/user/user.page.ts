import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StoredInfoService } from '../../service/stored-info.service';
import {StoredAccountService} from 'src/service/stored-account.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit{

  constructor(
    private storage: Storage,
    public storedInfo: StoredInfoService,
    private socialSharing: SocialSharing
  ) {
    if (!this.storedInfo.getName()){
      this.storedInfo.setName("Unknown");
      this.storage.set('name',this.storedInfo.getName());
    }
  }

  ngOnInit() {
  }
  sharing(){
    let address = StoredAccountService.address;
    let url = 'siriusid://siriusid.com/app/contact/' + address;
    this.socialSharing.share(url);
  }
}
