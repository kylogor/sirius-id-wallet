import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Platform, NavController, Config, PopoverController } from '@ionic/angular';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { Router } from '@angular/router';
import { MessageReceived, MessageType, CompressData, ApiNode } from 'siriusid-sdk';
import { TransactionParserService } from '../../service/transaction-parser.service';
import { DataForConfirmService } from '../../service/data-for-confirm.service';
import { TransactionType } from 'tsjs-xpx-chain-sdk';
import { ApiNodeService } from '../../service/api-node.service';
import { NotificationService } from 'src/service/notification.service';
import { AuthGuardCredentialReceivedService } from 'src/service/auth-guard-credential-received.service';
import { CredentialsService } from 'src/service/credentials.service';
import { SiriusDidService } from 'src/service/sirius-did.service';
import { DeeplinkDataService } from 'src/service/deeplink-data.service';
import { StoredAccountService } from 'src/service/stored-account.service';
import { AlertController } from '@ionic/angular';
import { AuthGuardCredentialDetailService } from 'src/service/auth-guard-credential-detail.service';
import { FoldersService } from "../../service/folders.service";
import { PopoverComponent } from '../popover/popover.component';
import { ɵangular_packages_platform_browser_platform_browser_k } from '@angular/platform-browser';


@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  scannedCode = null;
  error = false;
  loading = false;
  warning = false;
  allCredentials;
  isIOS = false

  res:any

  @ViewChild('fileInput', { static: true }) resultElement: ElementRef;
  elementType = 'url';
  value = 'https://assets.econsultancy.com/images/resized/0002/4236/qr_code-blog-third.png';
  showQRCode = false;
  fileToUpload: string | ArrayBuffer;
  ourFile: Blob;

  constructor(
    private apiNodeService: ApiNodeService,
    protected platform: Platform,
    protected navController: NavController,
    protected deeplinks: Deeplinks,
    private barcodeScanner: BarcodeScanner,
    private router: Router,
    private dataForConfirmService: DataForConfirmService,
    private credentialAuth: AuthGuardCredentialReceivedService,
    public credentialsService: CredentialsService,
    private siriusDidService: SiriusDidService,
    private deeplinkDataService: DeeplinkDataService,
    public deeplink: Deeplinks,
    private storedAccount: StoredAccountService,
    public alertController: AlertController,
    private authDetail: AuthGuardCredentialDetailService,
    private notiService: NotificationService,
    private config: Config,
    private renderer: Renderer2,
    public foldersService: FoldersService,
    public popoverController: PopoverController
  ) {}

  ngOnInit() {
    if (!this.siriusDidService.initialize){
      this.siriusDidService.init();
      this.siriusDidService.initialize = true;
    }
    
    this.config.set('animated', false);
    this.foldersService.getStorageFolders()
    setTimeout(() => {
      this.getAllFolderContent()
    }, 1500);

    if (this.platform.is('ios')) {
      this.isIOS = true
    } else {
      this.isIOS = false
    }

  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  async ionViewWillEnter() {
    console.log('VIEW WILL ENTER ---> main.page.ts');
    console.log('this.storedAccount.isOnBlockChain', this.storedAccount.isOnBlockChain);
    console.log('this.storedAccount.isSending', this.storedAccount.isSending);
    if (!this.storedAccount.isOnBlockChain && !this.storedAccount.isSending) {
      console.log('Account not available in blockchain');
      this.storedAccount.checkAccount();
      this.storedAccount.initAccount();
    } else { console.log('\n \n The account is on the blockchain\n \n '); }

    // Listen if message arrived
    this.deeplinkDataService.observableIsOnline.subscribe(async (messagge) => {
      console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{");
      let interval = await setInterval(() => {
        console.log("1");
        if (ApiNode.apiNode && this.credentialsService.start){
          this.deeplinkReceived(this.deeplinkDataService.messaggeReceived).then(() => {
            this.deeplinkDataService.messaggeReceived = null;
          });
          clearInterval(interval);
        }
      },1000);
      
    });
  }

  ionViewWillLeave() {
    this.loading = false;
  }

  /**
   *
   *
   * @param {string} data
   * @memberof MainPage
   */
  async deeplinkReceived(data: string) {
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");
    console.log(ApiNode.apiNode);
    console.log(this.credentialsService.credentials);
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++");
    
    console.log('\n\n ===== DATA RECEIVED ==== \n\n', data);
    if (data) {
      const newData = data.replace(new RegExp('-', 'g'), '/');
      CompressData.decompress(newData).then((result) => {
        console.log('decompress ---> ', result);
        if (result.length > 15) {
          this.loading = true;
          const messageReceived = new MessageReceived(result);
          console.log(messageReceived);
          this.error = false;
          this.dataForConfirmService.messageReceived = messageReceived;
          
          switch (messageReceived.message.type){
            case MessageType.TRANSACTION:
              this.transactionMessage(messageReceived);
              break;
            case MessageType.LOG_IN:
              this.loginAndVerifyMessage(messageReceived);
              break;
            case MessageType.CREDENTIAL:
              this.credentialMessage();
              break;
            case MessageType.CREDENTIAL_REQ:
              this.credentialReqMessage(messageReceived);
              break;
            case MessageType.VERIFY_BOTH:
              this.loginAndVerifyMessage(messageReceived);
                break;
            case MessageType.VERIFY_HARD:
              this.loginAndVerifyMessage(messageReceived);
              break;
            case MessageType.VERIFY_LIGHT:
              this.loginAndVerifyMessage(messageReceived);
              break;
          }

        } else {
          console.log('scanned failed!');
          this.error = true;
        }
      });
    }
  }

  /**
   *
   *
   * @memberof MainPage
   */
  async scanCode() {
    this.loading = true;
    this.warning = !(await ApiNodeService.checkNodeLive(ApiNodeService.apiNode));
    if (!this.warning) {
      this.barcodeScanner.scan().then(
        barcodeData => {
          this.loading = false;
          console.log('scannedCode');
          this.scannedCode = barcodeData.text;
          this.processCode();
        },
        err => console.log('Scan Error: ', err)
      );
    } else {
      this.loading = false;
      this.presentAlert();
    }
  }

  /**
   *
   *
   * @memberof MainPage
   */
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Your Internet connection is disable or Node Api was dead.',
      buttons: [
        {
          text: 'OK',
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ],
      cssClass: 'boxAlert'
    });

    await alert.present();
  }

  /**
   * @param {number} index
   * @memberof MainPage
   */
  credentialDetail(index: number) {
    for (let i=0;i<this.credentialsService.credentials.length;i++){
      if (this.credentialsService.credentials[i]['content']['id'] == this.credentialsService.credentialSingle[index]['content']['id']){
        this.credentialsService.credentialDetailIndex = i;
        break;
      }
      
    }
    // this.credentialsService.credentialDetailIndex = index;
    this.authDetail.auth = true;
    this.router.navigate(['/tabs/main/credential-detail']);
  }

  /**
   * @param {number} index
   * @memberof MainPage
   */
  credentialGroupDetail(index: number) {
    this.credentialsService.groupRoute = true; 
    this.credentialsService.credentialGroupIndex = index;
    // this.authDetail.auth = true;
    this.router.navigate(['/tabs/main/credential-group-detail']);
  }

  /**
   *
   *
   * @param {File} file
   * @param {*} $event
   * @memberof MainPage
   */
  fileChange(file: File, $event: any) {
    this.fileToUpload = '';
    if (file && file[0]) {
      if (file[0].type !== 'image/png') {
        console.log('invalid fomat');
      }
      this.ourFile = file[0];
      const reader = new FileReader();
      reader.readAsDataURL(this.ourFile);
      reader.onload = () => {
        this.fileToUpload = reader.result;
      };
    }
  }

  /**
   * Get array of intersection elements from 2 arrays
   * @param arr1
   * @param arr2
   */
  intersecArray(arr1: any[], arr2: string | any[]) {
    return arr1.filter((element1: { id: any; }) => arr2.includes(element1['content'].id));
  }
  /**
   *
   *
   * @memberof MainPage
   */
  navigate() {
    this.router.navigate(['/tabs/main/confirm']);
  }

  /**
   *
   *
   * @memberof MainPage
   */
  processCode() {
    CompressData.decompress(this.scannedCode).then((result) => {
      console.log(result);
      if (result.length > 15) {
        this.loading = true;
        const messageReceived = new MessageReceived(result);
        console.log(messageReceived);
        this.error = false;
        this.dataForConfirmService.messageReceived = messageReceived;

        switch (messageReceived.message.type){
          case MessageType.TRANSACTION:
            this.transactionMessage(messageReceived);
            break;
          case MessageType.LOG_IN:
            this.loginAndVerifyMessage(messageReceived);
            break;
          case MessageType.CREDENTIAL:
            this.credentialMessage();
            break;
          case MessageType.CREDENTIAL_REQ:
            this.credentialReqMessage(messageReceived);
            break;
          case MessageType.VERIFY_BOTH:
            this.loginAndVerifyMessage(messageReceived);
              break;
          case MessageType.VERIFY_HARD:
            this.loginAndVerifyMessage(messageReceived);
            break;
          case MessageType.VERIFY_LIGHT:
            this.loginAndVerifyMessage(messageReceived);
            break;
        }
        
      } else {
        console.log('scanned failed!');
        this.error = true;
      }
    });
  }

  /**
   *
   *
   * @param {{ result: string; }} e
   * @memberof MainPage
   */
  render(e: { result: string; }) {
    console.log(e.result);
    this.scannedCode = e.result;
    this.processCode();
    const element: Element = this.renderer.createElement('p');
    element.innerHTML = e.result;
    this.renderElement(element);
  }

  /**
   *
   *
   * @param {Element} element
   * @memberof MainPage
   */
  renderElement(element: Element) {
    for (const node of this.resultElement.nativeElement.childNodes) {
      this.renderer.removeChild(this.resultElement.nativeElement, node);
    }
    this.renderer.appendChild(this.resultElement.nativeElement, element);
  }

  /**
   *
   *
   * @memberof MainPage
   */
  openInput() {
    const inputFile = document.getElementById('fileInput');
    // tslint:disable-next-line:no-string-literal
    inputFile['value'] = '';
    inputFile.click();
  }

  elemRoute(item, index) {
    console.log(item);
    if (item.type === 'folder') {
      this.router.navigate([`/tabs/main/folder-content/${item.name}`])
    } else {
      this.credentialsService.credentialDetailIndex = index;
      this.authDetail.auth = true;
      this.router.navigate(['/tabs/main/credential-detail']);
    }
  }

  getAllFolderContent() {
    this.foldersService.allfolderContents()
    console.log(this.foldersService.allContents);
    this.allCredentials = this.foldersService.allContents
  }

  seeInfo() {
    this.router.navigate(['/tabs/main/confirm']);
  }

  loginAndVerifyMessage(messageReceived){
    console.log("---------------------------------");
    console.log(this.credentialsService.credentials);
    console.log(messageReceived.message.payload.credentials);
    console.log("---------------------------------");

    this.credentialsService.credentialsReq = this.intersecArray(
      this.credentialsService.credentials,
      messageReceived.message.payload.credentials
    );
    if (
      this.credentialsService.credentialsReq.length ===
      this.dataForConfirmService.messageReceived.message.payload.credentials.length
    ) {
      let credentialStored = new Array();
      for (let i=0;i<this.credentialsService.credentialsReq.length;i++){
        credentialStored[i] = this.credentialsService.credentialsReq[i]['stored'];
      }
      this.dataForConfirmService.messageReceived.message.payload.credentials = credentialStored;
      //Sau khi day du het cac credential dc yeu cau thi check xem co them dieu kien nua ko
      this.checkAdditionRequire(messageReceived.message.payload.addition);
      
    } else {
      this.credentialsService.notEnoughArray = new Array(
        this.dataForConfirmService.messageReceived.message.payload.credentials.length -
        this.credentialsService.credentialsReq.length
      );
      this.loading = false;
      this.router.navigate(['/tabs/main/confirm-error']);
    }
  }

  transactionMessage(messageReceived){
    console.log('transaction Type');
    if (messageReceived.message.payload.transaction) { // check transaction field not null
      if (messageReceived.message.payload.transaction.type === TransactionType.TRANSFER) { // transfer transaction
        TransactionParserService.parser(messageReceived.message.payload.transaction).then(tx => {
          this.dataForConfirmService.transaction = tx;
          this.navigate();
        });
      } else {// other transaction
        TransactionParserService.parser(messageReceived.message.payload.transaction).then(tx => {
          console.log('after parser');
          this.dataForConfirmService.transaction = tx;
          this.loading = false;
          this.router.navigate(['/tabs/main/confirm1']);
        });
      }
    } else {// cosignature transaction
      this.dataForConfirmService.transaction = null;
      this.loading = false;
      this.router.navigate(['/tabs/main/confirm1']);
    }
  }

  credentialMessage(){
    this.credentialAuth.auth = true;
    this.loading = false;
    this.router.navigate(['/credential-received']);
  }

  credentialReqMessage(messageReceived){
    this.notiService.listNotifications.push(messageReceived.message.payload);
    this.credentialAuth.auth = true;
    this.loading = false;
    this.router.navigate(['/credential-confirm']);
  }

  checkAdditionRequire(addition){
    if (addition && addition.length != 0){
      console.log("co additionnnnnnnnnnnn cccccccccccccccc");
      console.log(addition);
      this.router.navigate(['/tabs/main/addition-requires']);
    }
    else this.navigate();
  }
}