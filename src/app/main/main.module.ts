import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MainPage } from './main.page';

// import { NgxQRCodeModule } from 'ngx-qrcode2';

// import { NgQRCodeReaderModule } from 'ng2-qrcode-reader';
import { PopoverComponent } from '../popover/popover.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: MainPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    MainPage,
    PopoverComponent,
  ],
  entryComponents: [PopoverComponent]
})
export class MainPageModule {}
