import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPage } from './main.page';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import {UrlSerializer} from '@angular/router';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('MainPage', () => {
  let component: MainPage;
  let fixture: ComponentFixture<MainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        Location, 
        {provide: LocationStrategy, useClass: PathLocationStrategy},
        UrlSerializer,
        Deeplinks,
        BarcodeScanner,
        {provide: Router, useClass: MockRouter}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
