import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/service/contact.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit {

  detail;
  constructor(
    public contact: ContactService,
    private router: Router
  ) { 
    this.detail = this.contact.contactList[this.contact.contactDetailIndex];
  }

  ngOnInit() {
  }

  remove(){
    this.contact.removeContact(this.contact.contactDetailIndex);
    this.router.navigate(["/tabs/contact"]);
  }
}
