import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';

import { User1Page } from './user1.page';
import { Router } from '@angular/router';
import { Chooser } from '@ionic-native/chooser/ngx';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('User1Page', () => {
  let component: User1Page;
  let fixture: ComponentFixture<User1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ User1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        Chooser,
        { provide: Router, useClass: MockRouter }
      ],
      imports: [
        IonicStorageModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(User1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
