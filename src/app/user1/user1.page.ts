import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Router} from '@angular/router';
import { StoredInfoService } from '../../service/stored-info.service';
import { Chooser } from '@ionic-native/chooser/ngx';
import { SiriusDidService } from 'src/service/sirius-did.service';
import { IonContent } from '@ionic/angular';

import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActionSheetController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-user1',
  templateUrl: './user1.page.html',
  styleUrls: ['./user1.page.scss'],
})
export class User1Page implements OnInit {

  @ViewChild(IonContent,{static:false}) ionContent: IonContent;

  avatar:string = null;
  flag:boolean = false;
  error:boolean = false;
  errorText:string = null;
  avatarLink1 = "";
  avatarLink2 = "";
  loading = false;
  constructor(
    private router: Router,
    private storage: Storage,
    public storedInfo:StoredInfoService,
    private chooser: Chooser,
    // public siriusDidService:SiriusDidService,

    private camera: Camera, 
    private file: File, 
    private webView: WebView,
    private actionSheetController: ActionSheetController, 
    private platform: Platform, 
    private filePath: FilePath

    ) {
  }

  async update(){
    if (this.checkEmail(this.storedInfo.getEmail())){
      this.error = false;
      this.avatar = null;
      this.scrollContent();
      this.loading = true;
      this.storage.set('name',this.storedInfo.getName());
      this.storage.set('email',this.storedInfo.getEmail());
      this.storage.set('country',this.storedInfo.getCountry());
      this.storage.set('phoneNumber',this.storedInfo.getPhoneNumber());
      this.storage.set('avatar',this.storedInfo.avatar);
      await SiriusDidService.updateUserProfile();
      this.loading = false;
      SiriusDidService.getDid();
      this.navigate();
    }
    else {
      this.storedInfo.setEmail("");
      this.error = true;
      this.errorText = "Email Incorrect";
    }
  }

  async getImage(){
    this.flag = true;
    const file = await this.chooser.getFile("image/*");
    if (file){
      this.avatar = this.storedInfo.avatar;
      this.storedInfo.setAvatar(file.dataURI);
    }
    this.flag = false;
  }

  ngOnInit() {
  }

  navigate(){
    this.router.navigate(['/tabs/user/']);
  }

  ionViewWillLeave(){
    if (this.avatar){
      this.storedInfo.setAvatar(this.avatar);
    }
    if (this.error){
      this.storage.get("email").then((value) => {
        this.storedInfo.setEmail(value);
      })
      this.storage.get("country").then((value) => {
        this.storedInfo.setCountry(value);
      })
      this.storage.get("phoneNumber").then((value) => {
        this.storedInfo.setPhoneNumber(value);
      })
    }
    this.error = false;
  }

  checkEmail(email){
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
  }

  async updateTempAvatar() {
    const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.CAMERA);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
  }
 
  takePicture(sourceType: PictureSourceType) {
    this.flag = true;
    var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
    };
 
    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android')){
        if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath).then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
        } 
        else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
      }
      else if (this.platform.is('ios')){
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDirOnIos(correctPath, currentName, this.createFileName());
      }
      this.flag = false;
    });
    
  }

  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
  }
  
  copyFileToLocalDir(namePath, currentName, newFileName) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.avatar = this.storedInfo.avatar;
        this.storedInfo.setAvatar(this.webView.convertFileSrc(this.file.dataDirectory + newFileName));
      }, error => {
          console.log('Error while storing file.');
      });
  }

  copyFileToLocalDirOnIos(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.avatar = this.storedInfo.avatar;
      const win: any = window;
      const avatar = win.Ionic.WebView.convertFileSrc(this.file.dataDirectory + newFileName);
      this.storedInfo.setAvatar(avatar);
    }, error => {
        console.log('Error while storing file.');
    });
  }

  scrollContent() {
    this.ionContent.scrollToTop(300);
  }
  
}
