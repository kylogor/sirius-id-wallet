import { Component, OnInit } from '@angular/core';
import { CredentialsService } from '../../service/credentials.service';
import { Router } from '@angular/router';
import { AuthGuardCredentialDetailService } from 'src/service/auth-guard-credential-detail.service';
import { AlertController, Platform } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Menu2Component } from '../menu2/menu2.component';
import { FoldersService } from 'src/service/folders.service';
import { TagsService } from 'src/service/tags.service';

@Component({
  selector: 'app-credential-detail',
  templateUrl: './credential-detail.page.html',
  styleUrls: ['./credential-detail.page.scss'],
})
export class CredentialDetailPage implements OnInit {

  credential: object;
  isIOS: boolean = false

  constructor(
    private credentialsService: CredentialsService,
    private router: Router,
    private auth: AuthGuardCredentialDetailService,
    private alertController: AlertController,
    public popoverController: PopoverController,
    private folderService: FoldersService,
    private tagsService: TagsService,
    private platform: Platform
  ) {
    console.log('constructor credential-detail');
    this.credential = this.credentialsService.credentials[this.credentialsService.credentialDetailIndex]['content'];
    console.log(this.credential);
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Delete',
      message: 'You want to delete this credential?',
      cssClass: 'alertClass',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Delete',
          handler: () => {
            this.deleteCredential();
          }
        }
      ]
    });

    await alert.present();
  }

  async presentPopover(ev, data) {
    const popover = await this.popoverController.create({
      component: Menu2Component,
      event: ev,
      translucent: false
    });

    this.folderService.currentData(data)
    this.credentialsService.currentData(data)
    return await popover.present();
  }

  deleteCredential() {
    this.credentialsService.deleteCredential();
    this.auth.auth = false;
    if (this.credentialsService.groupRoute){
      this.router.navigate(['/tabs/main/credential-group-detail']);
    }
    else this.router.navigate(['/tabs/main']);
    
  }

  ngOnInit() {
    let btnDel = document.getElementById('btn-del');
    if (this.platform.is('ios')) {
      this.isIOS = true;
      btnDel.classList.add('btn-cont-ios');
    } else {
      this.isIOS = false;
      btnDel.classList.add('btn-cont');
    }
  }

  ionViewWillEnter() {
    console.log('enter credential-detail');
    this.credential = this.credentialsService.credentials[this.credentialsService.credentialDetailIndex]['content'];
    console.log(this.credential);
  }
}
