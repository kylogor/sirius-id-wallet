import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialDetailPage } from './credential-detail.page';

describe('CredentialDetailPage', () => {
  let component: CredentialDetailPage;
  let fixture: ComponentFixture<CredentialDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
