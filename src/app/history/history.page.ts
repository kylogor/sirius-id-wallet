import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular/';
import { Router } from '@angular/router';
import {HistoryService} from '../../service/history.service';
import {ApiNodeService} from '../../service/api-node.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage {

  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  type: any;
  recepient: any;
  signer: any;
  dateTime: any;
  i: any;
  mosaic: any;
  message: any;
  coinName: any;

  constructor( 
    private router: Router, 
    private historyService: HistoryService,
    private storage: Storage, 
    ){
    var publicKey : string;
    this.storage.get('publicKey').then((val) => {
      publicKey = val;
      this.type = [];
      this.recepient = [];
      this.signer = [];
      this.dateTime = [];
      this.mosaic = [];
      this.message = [];
      this.coinName = [];

      const api_url = ApiNodeService.apiNode;
      const NETWORK_TYPE = ApiNodeService.NETWORK_TYPE;
      var his = new HistoryService();
      his.getConfirmedTransaction(publicKey, api_url, NETWORK_TYPE).then(transaction => {
        for (let i = 0; i < his.lengthTx; i++) {
          this.type[i] = his.getMessageType(transaction[i]);
          this.signer[i] = his.getAdressSigner(transaction[(i)]);
          this.recepient[i] = his.getAddressRecipient(transaction[i]);
          this.dateTime[i] = his.getDateTime(transaction[i]);
          this.mosaic[i] = his.getMosaicSent(transaction[i]);
          this.message[i] = his.getMessage(transaction[i]);
          his.getCoinName(transaction[i]).then(result => {
            this.coinName[i] = result;
          });
        }
        // console.log(this.type);
        this.historyService.type = this.type;
        this.historyService.signer = this.signer;
        this.historyService.recepient = this.recepient;
        this.historyService.dateTime = this.dateTime;
        this.historyService.mosaic = this.mosaic;
        this.historyService.message = this.message;
        this.historyService.coinName = this.coinName;

      })
    })
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.recepient.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  onSelect(index: number) {
    this.i = index;
    this.historyService.i = this.i;
    if (this.type[this.i] == 1){
      this.router.navigate(['/tabs/main/transactionDetail']);
    }
    else {
      if (this.type[this.i] == 2){
        this.router.navigate(['/tabs/main/transactionDetail1']);
      }
    }
  }

}
