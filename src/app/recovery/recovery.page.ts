import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoredAccountService } from '../../service/stored-account.service';
import { Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { Bip39 } from 'siriusid-sdk';
import { ApiNodeService } from 'src/service/api-node.service';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.page.html',
  styleUrls: ['./recovery.page.scss'],
})
export class RecoveryPage implements OnInit {

  passPhrase: string = "";
  wordNumber: number = 0;
  constructor(
    private router: Router,
    private storedAccountService:StoredAccountService
    ) {

     }
  navigate() {
    this.router.navigate(['/recovery1'])
  }

  async storedPrivateKey() {
    try {
      const privateKey = Bip39.MnemonicToEntropy(this.passPhrase);
      const account = Account.createFromPrivateKey(privateKey, ApiNodeService.NETWORK_TYPE);
      StoredAccountService.setPrivateKey(account.privateKey);
      StoredAccountService.setPublicKey(account.publicKey);
      StoredAccountService.setAddress(account.address.plain());
      this.navigate();
    }
    catch{
      this.router.navigate(['/recovery3']);
    }

  }

  ngOnInit() {
  }

  countWord(){
    this.wordNumber = this.passPhrase.split(" ").length;
  }
}
