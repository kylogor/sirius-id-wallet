import { Component, OnInit } from '@angular/core';
import {AuthGuardConfirm3Service} from '../../service/auth-guard-confirm3.service';
import {AuthGuardConfirm4Service} from '../../service/auth-guard-confirm4.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm3',
  templateUrl: './confirm3.page.html',
  styleUrls: ['./confirm3.page.scss'],
})
export class Confirm3Page implements OnInit {

  constructor(
    private auth: AuthGuardConfirm3Service,
    private auth_processing: AuthGuardConfirm4Service,
    private router: Router
  ) { }

  ngOnInit() {
  }

  home(){
    this.auth.auth = false;
    this.auth_processing.auth = true;
    this.router.navigate(['/tabs/main']);
  }

}
