import { Component, OnInit } from '@angular/core';
import {SiriusDidService} from 'src/service/sirius-did.service';
import {Clipboard} from '@ionic-native/clipboard/ngx';
import { AlertController } from '@ionic/angular';
import {StoredAccountService} from 'src/service/stored-account.service';
import { HttpClient } from '@angular/common/http';
import { StoredInfoService } from 'src/service/stored-info.service';

@Component({
  selector: 'app-sirius-did',
  templateUrl: './sirius-did.page.html',
  styleUrls: ['./sirius-did.page.scss'],
})
export class SiriusDidPage implements OnInit {

  copied = false;
  flag = true;
  address;
  didDocument;

  constructor(
    public siriusDidService:SiriusDidService,
    private cliboard: Clipboard,
    public alertController: AlertController,
    public storedAccount: StoredAccountService,
    public storedInfo:StoredInfoService,
    private httpClient: HttpClient,
  ) { 
    this.didDocument = SiriusDidService.didDocument.toJSON();
    // console.log(JSON.stringify(this.didDocument));
  }

  ngOnInit() {
    // this.didDocumnent = SiriusDidService.didDocument;
  }

  ionViewWillEnter(){
    this.address = StoredAccountService.address;
    
  }
  // async presentAlertInput() {
  //   const alert = await this.alertController.create({
  //     header: 'Type New Address',
  //     inputs: [
  //       {
  //         name: 'address',
  //         type: 'text',
  //         placeholder: 'new address'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         cssClass: 'secondary',
  //         handler: (blah) => {
  //           console.log('Confirm Cancel: blah');
  //         }
  //       }, {
  //         text: 'Add',
  //         handler: (data) => {
  //           this.siriusDidService.addAddress(data.address);
  //           this.flag = !this.flag;
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }

  // addBtn(){
  //   this.presentAlertInput();
  // }

  // copyBtn(){
  //   this.copied = true;
  //   this.cliboard.copy(this.siriusDidService.toJSON());
  // }
}
