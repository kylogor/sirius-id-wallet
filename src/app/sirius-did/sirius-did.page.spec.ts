import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiriusDidPage } from './sirius-did.page';

describe('SiriusDidPage', () => {
  let component: SiriusDidPage;
  let fixture: ComponentFixture<SiriusDidPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiriusDidPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiriusDidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
