import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CredentialsService } from 'src/service/credentials.service';
import { FoldersService } from 'src/service/folders.service';
import { AuthGuardCredentialDetailService } from 'src/service/auth-guard-credential-detail.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-folder-content',
  templateUrl: './folder-content.page.html',
  styleUrls: ['./folder-content.page.scss'],
})
export class FolderContentPage implements OnInit {

  title = 'Folder'
  name: string = 'content'
  credentials = []
  isIOS: boolean = true

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private credentialsServices: CredentialsService,
    private foldersServices: FoldersService,
    private authDetail: AuthGuardCredentialDetailService,
    private platform: Platform
  ) {}

  ngOnInit() {
    if (this.platform.is('ios')) {
      this.isIOS = true
    } else {
      this.isIOS = false
    }

    console.log(this.activatedRoute.snapshot.params.name);
    this.name = this.activatedRoute.snapshot.params.name
    this.getCredentialOfFolder()
  }

  getCredentialOfFolder() {
    let folder = this.foldersServices.getFolderByName(this.name)
    console.log(folder);
    let folderContent = folder['content']
    console.log(folderContent);


    let allCredentials = this.credentialsServices.credentials
    console.log(this.credentials);

    if (allCredentials.length > 0){
      allCredentials.forEach(elem => {
        console.log("TEST",elem.id, folderContent.includes(elem.id) === true);
  
        if (folderContent.includes(elem.id) === true) {
          this.credentials.push(elem)
        }
      })
    }
  }

  credentialDetail(id) {
    let index = this.credentialsServices.getCredentialById(id)
    this.credentialsServices.credentialDetailIndex = index;
    this.authDetail.auth = true;
    this.router.navigate(['/tabs/main/credential-detail']);
  }
}
