import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FolderContentPage } from './folder-content.page';

describe('FolderContentPage', () => {
  let component: FolderContentPage;
  let fixture: ComponentFixture<FolderContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderContentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FolderContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
