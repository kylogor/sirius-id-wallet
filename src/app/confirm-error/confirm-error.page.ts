import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CredentialsService} from 'src/service/credentials.service';

@Component({
  selector: 'app-confirm-error',
  templateUrl: './confirm-error.page.html',
  styleUrls: ['./confirm-error.page.scss'],
})
export class ConfirmErrorPage implements OnInit {

  constructor(
    private router: Router,
    public credentialsService: CredentialsService
  ) { 

  }


  home(){
    this.router.navigate(['/tabs/main']);
  }

  ngOnInit() {
  }

}
