import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AuthGuardService} from '../../service/auth-guard.service';
import { Router } from '@angular/router';
import { Confirm2Page } from './confirm2.page';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('Confirm2Page', () => {
  let component: Confirm2Page;
  let fixture: ComponentFixture<Confirm2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Confirm2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthGuardService,
        { provide: Router, useClass: MockRouter }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Confirm2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
