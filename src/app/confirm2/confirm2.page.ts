import { Component, OnInit } from '@angular/core';
import {AuthGuardConfirm2Service} from '../../service/auth-guard-confirm2.service';
import {AuthGuardConfirm4Service} from '../../service/auth-guard-confirm4.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm2',
  templateUrl: './confirm2.page.html',
  styleUrls: ['./confirm2.page.scss'],
})
export class Confirm2Page implements OnInit {
  successTx; // check transfer transaction success or not
  constructor(
    private auth: AuthGuardConfirm2Service,
    private auth_processing: AuthGuardConfirm4Service,
    private router: Router,
    ) { }

  ngOnInit() {
  }

  home(){
    this.auth.auth = false;
    this.auth_processing.auth = true;
    this.router.navigate(['/tabs/main']);
  }

}
