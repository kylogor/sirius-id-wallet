import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../service/auth-guard.service';
import { AuthGuardConfirm2Service } from '../service/auth-guard-confirm2.service';
import { AuthGuardConfirm3Service } from '../service/auth-guard-confirm3.service';
import { AuthGuardConfirm4Service } from '../service/auth-guard-confirm4.service';
import { AuthGuardCredentialReceivedService } from '../service/auth-guard-credential-received.service';
import { AuthGuardCredentialConfirmService } from '../service/auth-guard-credential-confirm.service';
import { NotAuthGuardService } from '../service/not-auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule), canActivate: [ NotAuthGuardService ] },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'policy', loadChildren: './policy/policy.module#PolicyPageModule' },
  { path: 'recovery', loadChildren: './recovery/recovery.module#RecoveryPageModule' },
  { path: 'recovery1', loadChildren: './recovery1/recovery1.module#Recovery1PageModule'},
  { path: 'recovery2', loadChildren: './recovery2/recovery2.module#Recovery2PageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'signup1', loadChildren: './signup1/signup1.module#Signup1PageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [ AuthGuardService ] },
  { path: 'signup2', loadChildren: './signup2/signup2.module#Signup2PageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'recovery3', loadChildren: './recovery3/recovery3.module#Recovery3PageModule'},
  { path: 'signup3', loadChildren: './signup3/signup3.module#Signup3PageModule', canActivate: [ NotAuthGuardService ] },
  { path: 'confirm4', loadChildren: './confirm4/confirm4.module#Confirm4PageModule', canActivate: [ AuthGuardConfirm4Service ] },
  { path: 'result', loadChildren: './confirm2/confirm2.module#Confirm2PageModule', canActivate: [ AuthGuardConfirm2Service ] },
  { path: 'fail', loadChildren: './confirm3/confirm3.module#Confirm3PageModule', canActivate: [ AuthGuardConfirm3Service ] },
  {
    path: 'credential-received',
    loadChildren: './credential-received/credential-received.module#CredentialReceivedPageModule',
    canActivate: [ AuthGuardCredentialReceivedService ]
  },
  {
    path: 'credential-not-enough',
    loadChildren: './credential-not-enough/credential-not-enough.module#CredentialNotEnoughPageModule',
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'contact-received',
    loadChildren: './contact-received/contact-received.module#ContactReceivedPageModule',
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'credential-confirm',
    loadChildren: './credential-confirm/credential-confirm.module#CredentialConfirmPageModule',
    canActivate: [ AuthGuardCredentialConfirmService ]
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then( m => m.SearchPageModule)
  },
  // {
  //   path: 'addition-requires',
  //   loadChildren: () => import('./addition-requires/addition-requires.module').then( m => m.AdditionRequiresPageModule)
  // },
  // {
  //   path: 'credential-group-detail',
  //   loadChildren: () => import('./credential-group-detail/credential-group-detail.module').then( m => m.CredentialGroupDetailPageModule)
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
