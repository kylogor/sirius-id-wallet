import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CredentialNotEnoughPage } from './credential-not-enough.page';

const routes: Routes = [
  {
    path: '',
    component: CredentialNotEnoughPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CredentialNotEnoughPage]
})
export class CredentialNotEnoughPageModule {}
