import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/service/notification.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  interval;
  constructor(
    public notificationService: NotificationService,
    private router: Router
  ) { 
  }

  ngOnInit() {
  }
  ionViewDidEnter(){
    if (this.notificationService.numberNoti > 0){
      this.router.navigate(['/credential-confirm']);
    }
    this.interval = setInterval(() => {
      if (this.notificationService.numberNoti > 0){
        this.router.navigate(['/credential-confirm']);
      }
    },1000);
  }

  ionViewWillLeave(){
    clearInterval(this.interval);
  }
}
