import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { DataForConfirmService } from 'src/service/data-for-confirm.service';
import {CredentialsService} from 'src/service/credentials.service';
import {Router} from '@angular/router';
import {AuthGuardCredentialReceivedService} from 'src/service/auth-guard-credential-received.service';
import { MessageType, CredentialStored, Credentials } from 'siriusid-sdk';
import { StoredAccountService } from 'src/service/stored-account.service';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-credential-received',
  templateUrl: './credential-received.page.html',
  styleUrls: ['./credential-received.page.scss'],
})
export class CredentialReceivedPage implements OnInit {

  @ViewChild(IonContent,{static:false}) ionContent: IonContent;

  credential;
  dapp_id;
  confirmCredential = false;
  loading = false;
  constructor(
    private dataForConfirmService: DataForConfirmService,
    private credentialsService: CredentialsService,
    private router: Router,
    private auth: AuthGuardCredentialReceivedService,
    // private http: HttpClient
  ) {
    if (this.dataForConfirmService.messageReceived.message.type === MessageType.CREDENTIAL) {
      this.credential = this.dataForConfirmService.messageReceived.message.payload.credentials;
      this.dapp_id = this.credential.id;
    } else if (this.dataForConfirmService.messageReceived.message.type === MessageType.CREDENTIAL_REQ) {
      this.confirmCredential = true;
      this.credential = this.dataForConfirmService.messageReceived.message.payload.credential;
    }
  }

  cancelBtn() {
    this.navigate();
  }

  confirmBtn() {
    // todo
  }

  async saveBtn() {
    console.log("SaveBtn Click");
    this.scrollContent();
    this.loading = true;
    const credentialStored = await CredentialStored.create(<Credentials>this.credential,StoredAccountService.getPrivateKey());
    const credential = {
      'content': this.credential,
      'stored': credentialStored
    }

    let index = -1;

    //Check this credential was included or not
    for (let i = 0; i < this.credentialsService.credentials.length; i++) {
      if (this.credentialsService.credentials[i]['content']['id'] === credential['content']['id']) {
        index = i;
        console.log('co credential nay roi');
        break;
      }
    }

    //Update exist credential
    if (index !== -1) {
      this.credentialsService.credentials[index] = credential;
      this.credentialsService.updateCredentials(index);
    }
    
    //New credential
    else {
      this.credentialsService.addCredentials(credential);
    }
    console.log(this.credentialsService.getCredentials());
    this.loading = false;
    this.navigate();
  }

  navigate() {
    this.auth.auth = false;
    this.router.navigate(['/tabs/main']);
  }
  ngOnInit() {
  }

  scrollContent() {
    this.ionContent.scrollToTop(300);
  }
}
