import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialReceivedPage } from './credential-received.page';

describe('CredentialReceivedPage', () => {
  let component: CredentialReceivedPage;
  let fixture: ComponentFixture<CredentialReceivedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialReceivedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialReceivedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
