import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SuperheaderComponent } from './superheader/superheader.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

const components = [SuperheaderComponent]
const modules = [IonicModule, CommonModule]

@NgModule({
  declarations: [
    components,
  ],
  imports: [
    modules
  ],
  exports: [
    components,
    modules
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }