import { Component, OnInit, Input } from '@angular/core';
import { EventEmitter } from 'protractor';
import { Platform, NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import {CredentialsService} from 'src/service/credentials.service';
@Component({
  selector: 'app-superheader',
  templateUrl: './superheader.component.html',
  styleUrls: ['./superheader.component.scss'],
})
export class SuperheaderComponent implements OnInit {

  @Input () title: string = ''
  @Input () subtitle: string = ''
  @Input () back: boolean = false

  isIOS = false

  constructor(
    protected platform: Platform,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private group: CredentialsService
  ) {}

  ngOnInit() {
    if (this.platform.is('ios')) {
      this.isIOS = true
    } else {
      this.isIOS = false
    }
    console.log(this.group.groupRoute);
    console.log("XXXXXXXX", this.title, this.subtitle, this.back);

  }

  goBack() {
    let currentRoute = this.router.url
    console.log(currentRoute);

    switch (currentRoute) {

      case '/tabs/main/credential-detail':
        if (!this.group.groupRoute){
          this.router.navigate(['/tabs/main'])
        }
        else this.router.navigate(['/tabs/main/credential-group-detail'])
        break;

      case '/tabs/main/credential-group-detail':
        this.group.groupRoute = false;
        this.router.navigate(['/tabs/main'])
        break;

      case '/tabs/user/user1':
        this.router.navigate(['/tabs/user'])
        break;

      case '/tabs/user/qr':
        this.router.navigate(['/tabs/user'])
        break;

      case '/tabs/setting/nortification':
        this.router.navigate(['/tabs/setting'])
        break;

      case '/tabs/setting/backup':
        this.router.navigate(['/tabs/setting'])
        break;

      case '/tabs/setting/sirius-did':
        this.router.navigate(['/tabs/setting'])
        break;

      case '/tabs/setting/node-setting':
        this.router.navigate(['/tabs/setting'])
        break;

      case '/search':
        this.router.navigate(['/tabs/main'])
        break;

      case '/tabs/contact/contact-detail':
        this.router.navigate(['/tabs/contact'])
        break;

      default:
        break;
    }

  }

}
