import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';

import { MyQRcodePage } from './my-qrcode.page';
import {Clipboard} from '@ionic-native/clipboard/ngx'

xdescribe('MyQRcodePage', () => {
  let component: MyQRcodePage;
  let fixture: ComponentFixture<MyQRcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyQRcodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        IonicStorageModule.forRoot()
      ],
      providers: [
        Clipboard,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyQRcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
