import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Confirm4Page } from './confirm4.page';
import { IonicStorageModule } from '@ionic/storage';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
xdescribe('Confirm4Page', () => {
  let component: Confirm4Page;
  let fixture: ComponentFixture<Confirm4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot()
      ],
      declarations: [ Confirm4Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Router, useClass: MockRouter },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Confirm4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
