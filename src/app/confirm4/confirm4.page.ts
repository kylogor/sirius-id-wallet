import { Component, OnInit } from '@angular/core';
import {ApiNodeService} from '../../service/api-node.service';
import {DataForConfirmService} from '../../service/data-for-confirm.service';
import { Storage } from '@ionic/storage';
import {
  LoginTxResponse, MessageType, AnnounceTransaction
} from 'siriusid-sdk';
import {Router} from '@angular/router';
import {AuthGuardConfirm4Service} from '../../service/auth-guard-confirm4.service';
import {AuthGuardConfirm2Service} from '../../service/auth-guard-confirm2.service';
import {AuthGuardConfirm3Service} from '../../service/auth-guard-confirm3.service';
import {AuthGuardConfirmService} from '../../service/auth-guard-confirm.service';
import {AuthGuardConfirm1Service} from '../../service/auth-guard-confirm1.service';
import { AuthGuardCredentialConfirmService } from 'src/service/auth-guard-credential-confirm.service';
import {NotificationService} from 'src/service/notification.service';
import { StoredAccountService } from 'src/service/stored-account.service';
import io from 'socket.io-client';


@Component({
  selector: 'app-confirm4',
  templateUrl: './confirm4.page.html',
  styleUrls: ['./confirm4.page.scss'],
})
export class Confirm4Page implements OnInit {

  percent:number = 0;
  sender;
  transaction:any;
  transactionHash:string;
  successTx;
  MESSAGE_TYPE;
  count = 0;

  constructor(
    private dataForConfirmService:DataForConfirmService, 
    private storage:Storage,
    private router: Router,
    private auth: AuthGuardConfirm4Service,
    private auth_success: AuthGuardConfirm2Service,
    private auth_fail: AuthGuardConfirm3Service,
    private auth1: AuthGuardConfirmService,
    private auth2: AuthGuardConfirm1Service,
    private auth3: AuthGuardCredentialConfirmService,
    private notificationService: NotificationService

  ) {}

  sendTransaction(){
    switch (this.MESSAGE_TYPE){
      case MessageType.LOG_IN:
        this.loginAndVerifyMessage();
        break;
      case MessageType.TRANSACTION:
        this.transactionMessage();
        break;
      case MessageType.CREDENTIAL_REQ:
        this.credentialResMessage();
        break;
      case MessageType.VERIFY_BOTH:
        this.loginAndVerifyMessage();
        this.verifyLightMessage();
        break;
      case MessageType.VERIFY_HARD:
        this.loginAndVerifyMessage();
        break;
      case MessageType.VERIFY_LIGHT:
        this.verifyLightMessage();
        break;
      default: this.loginAndVerifyMessage();
    }
  }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log("processing page will enter");
    this.auth_success.auth = true;
    this.auth_fail.auth = true;

    this.sender = StoredAccountService.getPrivateKey();
    if (this.notificationService.confirmCredential){
      this.notificationService.confirmCredential = false;
      this.MESSAGE_TYPE = MessageType.CREDENTIAL_RES;
      this.transaction = this.notificationService.transaction;
    }
    else{
      this.transaction = this.dataForConfirmService.transaction;
      this.MESSAGE_TYPE = this.dataForConfirmService.messageType;
      this.transactionHash = this.dataForConfirmService.messageReceived.message.payload.transactionHash;
    }
    
    this.sendTransaction();

    let interval = setInterval(() => {
      if(this.successTx == false){
        clearInterval(interval);
        clearInterval(timeOut);
        this.router.navigate(['/fail']);
      }
      else if (this.percent < 100 && this.successTx == true){
        this.percent = 100;
        clearInterval(interval);
        clearInterval(timeOut);
        this.router.navigate(['/result']);
      }
      else if (this.percent == 100 && this.successTx == true){
        clearInterval(interval);
        clearInterval(timeOut);
        this.router.navigate(['/result']);
      }
      else if (this.percent < 74){
        this.percent += 1;
      }
      else if (this.percent >= 74 && this.percent < 80){
        if (this.count == 10){
          this.count = 0;
          this.percent += 1;
        }
        else this.count++;
      }
      else if (this.percent >= 80 && this.percent < 85){
        if (this.count == 20){
          this.count = 0;
          this.percent += 1;
        }
        else this.count++;
      }
      else if (this.percent >= 85 && this.percent < 98){
        if (this.count == 50){
          this.count = 0;
          this.percent += 1;
        }
        else this.count++;
      }
    },100)

    let timeOut = setInterval(() => {
      let error: boolean;
      error = !(ApiNodeService.checkNodeLive(ApiNodeService.apiNode));
      if (error){
        this.router.navigate(['/fail']);
        clearInterval(interval);
        clearInterval(timeOut);
      }
    },60000);
  }

  ionViewDidLeave(){
    this.auth.auth = false;
    this.auth1.auth = true;
    this.auth2.auth = true;
    this.auth3.auth = true;
  }

  loginAndVerifyMessage(){
    console.log('this.sender --->', this.dataForConfirmService.messageReceived)
    LoginTxResponse.message(
      this.sender, 
      ApiNodeService.apiNode, 
      ApiNodeService.NETWORK_TYPE, 
      this.dataForConfirmService.messageReceived
    ).then((done) =>{
      this.successTx = done;
      console.log('successTx = ' + this.successTx);
      this.dataForConfirmService.successTx = this.successTx;
    }).catch(() => {
      this.router.navigate(['/fail']);
    });
  }

  transactionMessage(){
    console.log("send transaction");
    if (this.transaction !== null){
      console.log("co transaction");
      console.log(this.transaction);
      AnnounceTransaction.announce(this.transaction,this.sender,ApiNodeService.apiNode,ApiNodeService.NETWORK_TYPE).then(done => {
        this.successTx = done;
        console.log('successTx = ' + this.successTx);
        this.dataForConfirmService.successTx = this.successTx;
      }).catch((error) => {
        console.log(error);
        this.router.navigate(['/fail']);
      });
    }
    else {
      console.log("send cosignature transaction");
      console.log(this.dataForConfirmService);
      console.log(this.transactionHash);
      AnnounceTransaction.announceCosignature(this.transactionHash,this.sender,ApiNodeService.apiNode,ApiNodeService.NETWORK_TYPE).then(done => {
        this.successTx = done;
        console.log('successTx = ' + this.successTx);
        this.dataForConfirmService.successTx = this.successTx;
      }).catch((error) => {
        console.log(error);
        this.router.navigate(['/fail']);
      });
    }
  }

  credentialResMessage(){
    console.log("send credential confirm message");
    AnnounceTransaction.announce(this.transaction,this.sender,ApiNodeService.apiNode,ApiNodeService.NETWORK_TYPE).then(done => {
      this.successTx = done;
      console.log('successTx = ' + this.successTx);
      this.dataForConfirmService.successTx = this.successTx;
    }).catch((error) => {
      console.log(error);
      this.router.navigate(['/fail']);
    });
  }

  verifyLightMessage(){
    const url = this.dataForConfirmService.messageReceived.message.getUrl();
    const channel = this.generateToken();
    const socket = io(url);
    socket.connect();
    const data = {
      session: this.dataForConfirmService.messageReceived.message.getSessionToken(),
      publicKey: StoredAccountService.getPublicKey(),
      networkType:ApiNodeService.NETWORK_TYPE,
      credentials: this.dataForConfirmService.messageReceived.message.payload.credentials,
      channel:channel
    }

    console.log("+++++++++++ dataaaaaaaaa +++++++++++++++++++++++");
    console.log(data);
    console.log("+++++++++++ dataaaaaaaaa +++++++++++++++++++++++");

    console.log("emit successfully");
    socket.emit('verify',data);
    socket.on(channel, data => {
      this.successTx = data;
      this.dataForConfirmService.successTx = this.successTx;
    })

    socket.on("connect_failed", data => {
      console.log("connect to standard server io failed");
      this.successTx = false;
      this.dataForConfirmService.successTx = this.successTx;
    })

    socket.on("connect_error", data => {
      console.log("connect to standard server io error");
      this.successTx = false;
      this.dataForConfirmService.successTx = this.successTx;
    })
  }

  /**
   * Generate random string of sesion token
   */
  private generateToken() {
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 64; i++) {
        outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
    }
    return outString;
}
}
