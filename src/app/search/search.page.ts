import { Component, OnInit } from '@angular/core';
import { CredentialsService } from 'src/service/credentials.service';
import { TagsService } from 'src/service/tags.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  searchValue = ''
  allCredentials = []
  mainListActive = true
  backArrow = true

  constructor(
    private credentialService: CredentialsService,
    public tagsService: TagsService,
    private credentialsService: CredentialsService,
    private router: Router
  ) {}

  ngOnInit() {
  }

  inputChange() {
    let tmpArray = []
    let credentials = this.credentialService.credentials
    if (this.mainListActive === true) {
      console.log("1");
      credentials.forEach(elem => {
        if (this.searchValue !== '') {
          // let id = (elem['content'].id.search(this.searchValue) === -1) ? false : true
          let name = (elem['content'].name.toLowerCase().search(this.searchValue) === -1) ? false : true
          let description = (elem['content'].description.toLowerCase().search(this.searchValue) === -1) ? false : true

          let total = [name, description]

          if (total.includes(true)) {
            tmpArray.push(elem);
            console.log(tmpArray);
          }
        }
      })
    } else {
      let filtered = credentials.filter(elem => {
        for (let i = 0; i < this.tagsService.activeTag.length; i++) {
          console.log(this.tagsService.activeTag[i].name)
          console.log(elem.tags[i])
          console.log(elem.tags)

          if (elem.tags.includes(this.tagsService.activeTag[i].name)) {
            return elem
          }

        }
      })

      filtered.forEach(elem => {
        if (this.searchValue !== '') {
          let id = (elem.id.search(this.searchValue) === -1) ? false : true
          let name = (elem.name.search(this.searchValue) === -1) ? false : true
          let description = (elem.description.search(this.searchValue) === -1) ? false : true

          let total = [id, name, description]

          if (total.includes(true)) {
            tmpArray.push(elem)
          }
        } else {
          tmpArray = filtered
        }
      })
    }

    this.allCredentials = tmpArray
  }

  activateTag(name) {
    this.tagsService.activateTag(name)
    this.mainListActive = false
    console.log(this.tagsService.activeTag);

    if (this.tagsService.activeTag.length === 0) {
      this.mainListActive = true
    }

    this.inputChange()
  }

  turnOffTags() {
    this.mainListActive = true
    this.tagsService.turnOffTags()
    this.inputChange()
  }

  credentialDetail(index: number) {
    this.credentialsService.credentialDetailIndex = index;
    // this.authDetail.auth = true;
    this.router.navigate(['/tabs/main/credential-detail']);
  }

  clean(){
    this.searchValue = "";
    this.inputChange();
  }
}
