import { Component, OnInit } from '@angular/core';
import { FoldersService } from 'src/service/folders.service';
import { CredentialsService } from 'src/service/credentials.service';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { TagsService } from 'src/service/tags.service';

@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.scss'],
})
export class Menu2Component implements OnInit {
  mainActive = true
  showFolders = false
  showTags = false
  addTagActive = false
  folders
  tagName

  constructor(
    private folderService: FoldersService,
    private credentialService: CredentialsService,
    private activatedRoute: ActivatedRoute,
    private popoverController: PopoverController,
    public tagsServices: TagsService
  ) {}

  ngOnInit() {
    console.log(this.tagsServices.tags);
    this.folders = this.folderService.folders
    console.log("CREDENTIALS", this.credentialService.credentials)
    console.log(this.mainActive);
    console.log(this.showFolders);
    console.log(this.showTags);
  }

  showFolderList() {
    this.mainActive = false
    this.showFolders = true
  }

  showTagsList() {
    this.mainActive = false
    this.showTags = true
  }

  showAddTag() {
    this.showTags = false
    this.addTagActive = true
  }

  addContentToFolder(folderName, data = null) {
    if (data === null) {
      data = this.folderService.tmpData
    }
    this.folderService.addContent(folderName, data)
    this.folderService.clearCurrentData()
    console.log(this.folderService.folders);
    this.popoverController.dismiss()
  }

  addNewTag() {
    this.tagsServices.addTag(this.tagName)
    this.popoverController.dismiss()
  }

  assignTag(name) {
    let data = this.credentialService.currentCredential
    let allCred = this.credentialService.credentials
    let updateCred = []
    allCred.forEach(elem => {
      console.log( "AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII", elem.id === data);

      if (elem.id === data) {
        console.log(elem.tags);
        if (elem.tags === undefined) {
          elem.tags = [name]
        } else {
          if (elem.tags.includes(name) === false) {
            elem.tags.push(name)
          }
        }

        console.log(elem.tags)
      }
      updateCred.push(elem)
    })

    this.credentialService.credentials = updateCred
    console.log(updateCred);

    // this.credentialService.updateCredentials()
  }
}
