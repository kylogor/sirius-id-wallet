import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {ApiNodeService} from '../../service/api-node.service';
import {NodeHttp} from 'tsjs-xpx-chain-sdk';
import {NotificationService} from 'src/service/notification.service';
@Component({
  selector: 'app-node-setting',
  templateUrl: './node-setting.page.html',
  styleUrls: ['./node-setting.page.scss'],
})
export class NodeSettingPage implements OnInit {

  nodeList = [];
  otherNodes = [];
  currentNode;
  showBox:boolean = false;
  newNode;
  cssCustom:boolean = false;

  constructor(
    private storage:Storage,
    private notificationService: NotificationService  
  ) {}

  ionViewWillEnter(){
    this.nodeList = ApiNodeService.nodeList;
    this.otherNodes = ApiNodeService.otherNodes;
    this.currentNode = ApiNodeService.currentNode;
  }

  async getNodeInfo(node):Promise<boolean>{
    console.log("getNodeInfo");
    let nodeHttp = new NodeHttp(node);

    return new Promise((resolve) => {
      nodeHttp.getNodeInfo().subscribe(nodeInfo => {
        resolve(true);
      }, error => {
        resolve(false);
        console.log(error);
      }, () => {
          console.log("Done");
      });
    })
  }

  async setNode(url:string){
    console.log("setNode");
    console.log(url);
    let index1 = null;
    for (let i=0;i<this.nodeList.length;i++){
      if (this.nodeList[i].url == url){
        this.nodeList[i].chosen = true;
        index1 = i;

        this.currentNode.index = i;
        this.currentNode.otherNode = false;
        ApiNodeService.nodeList[i].chosen = true;
        ApiNodeService.nodeList[i].lived = this.nodeList[i].lived;
        // if (!ApiNodeService.checkHttp(this.nodeList[i].url)){
        //   ApiNodeService.apiNode = "https://" + this.nodeList[i].url;
        // }
        // else ApiNodeService.apiNode = this.nodeList[i].url;
        ApiNodeService.apiNode = this.nodeList[i].url;
        console.log("apiNodeeeeeeeeeee");
        console.log(ApiNodeService.apiNode);
      }
      else {
        this.nodeList[i].chosen = false;
        this.nodeList[i].lived = false;
      }
    }
    
    let index2 = null;
    for (let i=0;i<this.otherNodes.length;i++){
      if (this.otherNodes[i].url == url){
        this.otherNodes[i].chosen = true;
        index2 = i;

        this.currentNode.index = i;
        this.currentNode.otherNode = true;
        ApiNodeService.otherNodes[i].chosen = true;
        ApiNodeService.otherNodes[i].lived = this.otherNodes[i].lived;
        // if (!ApiNodeService.checkHttp(this.nodeList[i].url)){
        //   ApiNodeService.apiNode = "https://" + this.otherNodes[i].url;
        // }
        // else ApiNodeService.apiNode = this.otherNodes[i].url;
        ApiNodeService.apiNode = this.otherNodes[i].url;
        console.log("apiNodeeeeeeeeeee");
        console.log(ApiNodeService.apiNode);
      }
      else {
        this.otherNodes[i].chosen = false;
        this.otherNodes[i].lived = false;
      }
    }
    if (index1 !== null){
      this.nodeList[index1].lived = await ApiNodeService.checkNodeLive(url);
    }
    else if(index2 !== null){
      this.otherNodes[index2].lived = await ApiNodeService.checkNodeLive(url);
    }
    this.notificationService.updateWs();
    this.storage.set("currentNode",this.currentNode);
    this.storage.set("otherNodes",this.otherNodes);
  }

  async addNode(){
    let node = {
      url:this.newNode,
      chosen: true,
      lived: false
    };
    this.newNode = "";
    this.otherNodes.push(node);
    this.setNode(node.url);
    this.boxToggle();
  }

  boxToggle(){
    this.newNode = "";
    this.showBox = !this.showBox;
  }

  ionViewDidEnter(){
    this.cssCustom = true;
  }
  ngOnInit() {
  }

}
