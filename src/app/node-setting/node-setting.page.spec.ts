import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeSettingPage } from './node-setting.page';

describe('NodeSettingPage', () => {
  let component: NodeSettingPage;
  let fixture: ComponentFixture<NodeSettingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeSettingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeSettingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
