import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { StoredInfoService } from '../../service/stored-info.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  name = '';
  checkBoxList: any;
  complete = false;
  constructor(private router: Router, private storage: Storage, private storedInfo: StoredInfoService) {
    this.checkBoxList = [
      {
        value: 'Accept terms & conditions',
        isChecked: false,
        link: '/terms'
      },
      {
        value: 'Accept privacy policy',
        isChecked: false,
        link: '/policy'
      }
    ];
  }

  checkEvent() {
    console.log(this.name);
    const totalItems = this.checkBoxList.length;
    let checked = 0;
    this.checkBoxList.map(obj => {
      if (obj.isChecked) { checked++; }
    });
    if (checked === totalItems && this.name !== '') {
      this.complete = true;
      this.storedInfo.setName(this.name);
      this.storage.set('name', this.name);
    } else { this.complete = false; }
  }

  navigate() {
    this.router.navigate(['/signup1']);
    this.router.navigate(['/terms']);
    this.router.navigate(['/policy']);
  }

  ngOnInit() {
  }

}
