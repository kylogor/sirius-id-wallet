import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NotAuthGuardService } from '../service/not-auth-guard.service';
import { AuthGuardConfirm2Service } from '../service/auth-guard-confirm2.service';
import { AuthGuardConfirm3Service } from '../service/auth-guard-confirm3.service';
import { AuthGuardConfirm4Service } from '../service/auth-guard-confirm4.service';
import { AuthGuardCredentialReceivedService } from '../service/auth-guard-credential-received.service';
import { AuthGuardCredentialConfirmService } from 'src/service/auth-guard-credential-confirm.service';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule), canActivate: [NotAuthGuardService] },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'policy', loadChildren: './policy/policy.module#PolicyPageModule' },
  { path: 'recovery', loadChildren: './recovery/recovery.module#RecoveryPageModule' },
  { path: 'recovery1', loadChildren: './recovery1/recovery1.module#Recovery1PageModule' },
  { path: 'recovery2', loadChildren: './recovery2/recovery2.module#Recovery2PageModule', canActivate: [NotAuthGuardService] },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'signup1', loadChildren: './signup1/signup1.module#Signup1PageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'signup2', loadChildren: './signup2/signup2.module#Signup2PageModule', canActivate: [NotAuthGuardService] },
  { path: 'recovery3', loadChildren: './recovery3/recovery3.module#Recovery3PageModule' },
  { path: 'signup3', loadChildren: './signup3/signup3.module#Signup3PageModule' },
  { path: 'confirm4', loadChildren: './confirm4/confirm4.module#Confirm4PageModule', canActivate: [AuthGuardConfirm4Service] },
  { path: 'result', loadChildren: './confirm2/confirm2.module#Confirm2PageModule', canActivate: [AuthGuardConfirm2Service] },
  { path: 'fail', loadChildren: './confirm3/confirm3.module#Confirm3PageModule', canActivate: [AuthGuardConfirm3Service] },
  // { path: 'credential-detail', loadChildren: './credential-detail/credential-detail.module#CredentialDetailPageModule' },
  {
    path: 'credential-received',
    loadChildren: './credential-received/credential-received.module#CredentialReceivedPageModule',
    canActivate: [AuthGuardCredentialReceivedService]
  },
  {
    path: 'credential-not-enough',
    loadChildren: './credential-not-enough/credential-not-enough.module#CredentialNotEnoughPageModule'
  },
  {
    path: 'contact-received',
    loadChildren: './contact-received/contact-received.module#ContactReceivedPageModule'
  },
  {
    path: 'credential-confirm',
    loadChildren: './credential-confirm/credential-confirm.module#CredentialConfirmPageModule',
    canActivate: [AuthGuardCredentialConfirmService]
  },
  // { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' },

  // { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  // { path: 'sirius-did', loadChildren: './sirius-did/sirius-did.module#SiriusDidPageModule' },
  // { path: 'confirm-error', loadChildren: './confirm-error/confirm-error.module#ConfirmErrorPageModule' },
  // { path: 'credentials', loadChildren: './credentials/credentials.module#CredentialsPageModule' },
  // { path: 'node-setting', loadChildren: './node-setting/node-setting.module#NodeSettingPageModule' },
  // { path: 'confirm3', loadChildren: './confirm3/confirm3.module#Confirm3PageModule' },
  // { path: 'confirm4', loadChildren: './confirm4/confirm4.module#Confirm4PageModule' },
  // { path: 'backup', loadChildren: './backup/backup.module#BackupPageModule' },
  // { path: 'nortification', loadChildren: './nortification/nortification.module#NortificationPageModule' },
  // { path: 'transaction-detail1', loadChildren: './transaction-detail1/transaction-detail1.module#TransactionDetail1PageModule' },
  // { path: 'transaction-detail', loadChildren: './transaction-detail/transaction-detail.module#TransactionDetailPageModule' },
  // { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  // { path: 'camera', loadChildren: './camera/camera.module#CameraPageModule' },
  // { path: 'user1', loadChildren: './user1/user1.module#User1PageModule' },
  // { path: 'my-qrcode', loadChildren: './my-qrcode/my-qrcode.module#MyQRcodePageModule' },
  // { path: 'confirm1', loadChildren: './confirm1/confirm1.module#Confirm1PageModule' },
  // { path: 'confirm2', loadChildren: './confirm2/confirm2.module#Confirm2PageModule' },
  // { path: 'confirm', loadChildren: './confirm/confirm.module#ConfirmPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
