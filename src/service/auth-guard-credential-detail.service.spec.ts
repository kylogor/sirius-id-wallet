import { TestBed } from '@angular/core/testing';

import { AuthGuardCredentialDetailService } from './auth-guard-credential-detail.service';

describe('AuthGuardCredentialDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardCredentialDetailService = TestBed.get(AuthGuardCredentialDetailService);
    expect(service).toBeTruthy();
  });
});
