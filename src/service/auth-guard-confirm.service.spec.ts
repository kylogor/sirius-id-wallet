import { TestBed } from '@angular/core/testing';

import { AuthGuardConfirmService } from './auth-guard-confirm.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardConfirmService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: Router, useClass: MockRouter}
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardConfirmService = TestBed.get(AuthGuardConfirmService);
    expect(service).toBeTruthy();
  });
});
