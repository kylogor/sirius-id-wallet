import { TestBed } from '@angular/core/testing';

import { AuthGuardConfirm4Service } from './auth-guard-confirm4.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardConfirm4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: Router, useClass: MockRouter}
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardConfirm4Service = TestBed.get(AuthGuardConfirm4Service);
    expect(service).toBeTruthy();
  });
});
