import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
@Injectable({
  providedIn: 'root'
})
export class StoredInfoService {

  name: string;
  email: string;
  country: string;
  phoneNumber: string;
  avatar: string;

  constructor(
    private storage: Storage,
    private sanitize: DomSanitizer,
    private platform: Platform
  ) {
    this.storage.get('name').then((val) => {
      this.name = val;
    });

    this.storage.get('email').then((val) => {
      this.email = val;
    });

    this.storage.get('country').then((val) => {
      this.country = val;
    });

    this.storage.get('phoneNumber').then((val) => {
      this.phoneNumber = val;
    });

    this.storage.get('avatar').then((val) => {
      if (val) {
        this.avatar = val;
      } else { this.avatar = 'assets/user.png'; }
    });
  }

  setName(name: string) {
    this.name = name;
  }

  getName() {
    return this.name;
  }

  setEmail(email: string) {
    this.email = email;
  }

  getEmail() {
    return this.email;
  }

  setCountry(country: string) {
    this.country = country;
  }

  getCountry() {
    return this.country;
  }

  setPhoneNumber(phoneNumber: string) {
    this.phoneNumber = phoneNumber;
  }

  getPhoneNumber() {
    return this.phoneNumber;
  }

  setAvatar(avatar: string) {
    this.avatar = avatar;
  }

  getAvatar() {
    if (this.platform.is('android')) {
      return this.avatar;
    } else if (this.platform.is('ios')) {
      return this.sanitize.bypassSecurityTrustResourceUrl(this.avatar);
    }
  }

}
