import { TestBed } from '@angular/core/testing';

import { HistoryService } from './history.service';
import { NetworkType } from 'tsjs-xpx-chain-sdk';

describe('HistoryService', () => {
  let service: HistoryService;
  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.get(HistoryService);
  });

  it('should be created',()=>{
    expect(service).toBeTruthy();
  });


  it('getConfirmedTransaction', async (done)=>{
    // const api_url = 'http://bctestnet1.xpxsirius.io:3000';
    const api_url = 'https://bctestnet1.brimstone.xpxsirius.io';
    const publicKey = '990585BBB7C97BB61D90410B67552D82D30738994BA7CF2B1041D1E0A6E4169B'

    const list = await service.getConfirmedTransaction(publicKey, api_url, NetworkType.TEST_NET)

    expect(list.length).toBeGreaterThan(0)
    done()
  })

});