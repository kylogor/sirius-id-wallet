import { TestBed } from '@angular/core/testing';

import { AuthGuardConfirm3Service } from './auth-guard-confirm3.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardConfirm3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: Router, useClass: MockRouter}
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardConfirm3Service = TestBed.get(AuthGuardConfirm3Service);
    expect(service).toBeTruthy();
  });
});
