import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardCredentialDetailService implements CanActivate{

  auth = true;
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    console.log(route);
    if (!this.auth) {
      return false;
    }
    return true;

  }
}
