import { TestBed } from '@angular/core/testing';

import { AuthGuardConfirm2Service } from './auth-guard-confirm2.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardConfirm2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: Router, useClass: MockRouter}
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardConfirm2Service = TestBed.get(AuthGuardConfirm2Service);
    expect(service).toBeTruthy();
  });
});
