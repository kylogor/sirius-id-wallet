import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FoldersService {

  folders: object[] = new Array()
  tmpData: any = null
  allContents = []

  constructor(
    private storage: Storage,
    // private folderSubcribe: BehaviorSubject<object[]> = new BehaviorSubject<object[]>(null),
    // private folderObservable: Observable<object[]> = this.folderSubcribe.asObservable()
  ) {}

  newFolder(name) {
    let newFolder = {
      name: name,
      content: [],
      type: 'folder'
    }

    let result = null

    if (this.folders !== null) {
      result = this.folders.find(el => el['name'] === name)
    }

    if ([undefined, null].includes(result)) {
      if (this.folders === null) {
        this.folders = []
      }
      this.folders.push(newFolder)
    } else {
      console.warn("Folder exist");
    }

    this.storage.set('customFolders', this.folders)
    console.log("RESULT: ", result);
  }

  getFolderByName(name) {
    let result = this.folders.find(el => el['name'] === name)

    console.log(result)

    if (result !== undefined) {
    } else {
      console.warn(`The folder ${name} no exist`)
      result = null
    }

    return result
  }

  async getStorageFolders() {
    let tmpFolder = await this.storage.get('customFolders')
    console.log(tmpFolder);
    this.folders = tmpFolder
  }

  addContent(folderName, data) {
    let currentFolder = this.getFolderByName(folderName)
    currentFolder['content'].push(data)
    this.storage.set('customFolders', this.folders)
    this.allfolderContents()
  }

  currentData(data) {
    this.tmpData = data
  }

  clearCurrentData() {
    this.tmpData = null
  }

  allfolderContents() {
    if (this.folders){
      this.folders.forEach(elem => {
        elem['content'].forEach(elem => {
          if (this.allContents.includes(elem) === false) {
            this.allContents.push(elem)
            console.log(this.allContents);
          }
        })
      })
    }
  }
}

