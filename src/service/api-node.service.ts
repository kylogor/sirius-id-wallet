import { Injectable } from '@angular/core';
import { NetworkType, NodeHttp, Account, Address } from 'tsjs-xpx-chain-sdk';
import { Storage } from '@ionic/storage';
import { StoredAccountService } from './stored-account.service';
import { ApiNode } from 'siriusid-sdk';
import { SiriusDidService } from 'src/service/sirius-did.service';
@Injectable({
  providedIn: 'root'
})
export class ApiNodeService {

  // static apiNode = "https://bctestnet1.brimstone.xpxsirius.io";
  static apiNode: string;
  static NETWORK_TYPE = NetworkType.TEST_NET; //default api node

  static nodeList = [
    {
      url: 'https://demo-sc-api-1.ssi.xpxsirius.io',
      chosen: false,
      lived: false
    },
    {
      url: 'https://demo-sc-api-2.ssi.xpxsirius.io',
      chosen: false,
      lived: false
    }
    // {
    //   url: 'https://bctestnet2.brimstone.xpxsirius.io',
    //   chosen: false,
    //   lived: false
    // }
  ];

  static otherNodes = [];
  static currentNode = {
    otherNode: false,
    index: 0
  };

  constructor(private storage: Storage, private siriusDidService: SiriusDidService) {
    console.log("co chay api node service");
    this.storage.get('otherNodes').then((val) => {
      console.log('SEARCH OTHER NODES ----> ', val);
      if (val) {
        ApiNodeService.otherNodes = val;
      }

      this.storage.get('currentNode').then((currentNode) => {
        console.log('CURRENT NODE ----->', currentNode);
        if (currentNode) {
          ApiNodeService.currentNode = currentNode;
        }

        console.log('------ SET API NODE ------');
        ApiNodeService.setApiNode();
        ApiNodeService.checkCurrentNode();
        ApiNode.apiNode = ApiNodeService.apiNode;
        ApiNode.networkType = ApiNodeService.NETWORK_TYPE;
      });
    });
  }

  /**
   *
   *
   * @static
   * @returns
   * @memberof ApiNodeService
   */
  static setApiNode() {
    console.log('=== SET API NODE FUNCTION ===');
    console.log('OTHER NODES --> ', this.otherNodes);
    console.log('CURRENT NODES --> ', this.currentNode);
    if (this.currentNode.otherNode) {
      const i = this.currentNode.index;
      // if (!this.checkHttp(this.otherNodes[i].url)) {
      //   this.apiNode = 'https://' + this.otherNodes[i].url;
      //   return;
      // }

      this.apiNode = this.otherNodes[i].url;
    } else {
      const i = this.currentNode.index;
      // if (!this.checkHttp(this.nodeList[i].url)) {
      //   this.apiNode = 'https://' + this.nodeList[i].url;
      //   return;
      // }

      this.apiNode = this.nodeList[i].url;
    }
  }

  /**
   *
   *
   * @static
   * @memberof ApiNodeService
   */
  static async checkCurrentNode() {
    if (this.currentNode.otherNode) { // current node is belong to other node list
      const i = this.currentNode.index;
      this.otherNodes[i].chosen = true;
      this.checkNodeLive(this.otherNodes[i].url);
      this.nodeList[i].lived = await this.checkNodeLive(this.nodeList[i].url);
    } else { // current node is belong to default node list
      console.log('nodeList');
      const i = this.currentNode.index;
      this.nodeList[i].chosen = true;
      this.nodeList[i].lived = await this.checkNodeLive(this.nodeList[i].url);
      console.log(this.nodeList);
    }
  }

  /**
   *
   *
   * @static
   * @param {string} url
   * @returns {Promise<boolean>}
   * @memberof ApiNodeService
   */
  static async checkNodeLive(url: string): Promise<boolean> {
    console.log('===== CHECK NODE LIVE =====');
    let nodeHttp: NodeHttp;
    nodeHttp = new NodeHttp(url);
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(false);
      }, 2000);

      nodeHttp.getNodeInfo().subscribe(nodeInfo => {
        if (ApiNodeService.NETWORK_TYPE != <NetworkType>nodeInfo.networkIdentifier){
          console.log("chain was changed");
          ApiNodeService.NETWORK_TYPE = <NetworkType>nodeInfo.networkIdentifier;
          ApiNode.apiNode = url;
          ApiNode.networkType = ApiNodeService.NETWORK_TYPE;
          const newAddress = Address.createFromPublicKey(StoredAccountService.getPublicKey(),ApiNodeService.NETWORK_TYPE);
          StoredAccountService.setAddress(newAddress.plain());
          SiriusDidService.recoveryDid();
        }
        else {
          console.log("chain wasn't changed");
          ApiNode.apiNode = url;
        }
        resolve(true);
      }, error => {
        console.log(error);
        resolve(false);
      }, () => {
        console.log('Done');
      });
    });
  }

  // static checkHttp(url: string) {
  //   if (url) {
  //     const result = url.indexOf('http');
  //     if (result == -1) {
  //       return false;
  //     } else { return true; }
  //   } else { return false; }
  // }

  // static checkHttps(url: string) {
  //   if (url) {
  //     const result = url.indexOf('https');
  //     if (result == -1) {
  //       return false;
  //     } else { return true; }
  //   } else { return false; }
  // }

  /**
   *
   *
   * @memberof ApiNodeService
   */
  start() {
    console.log('START API NODE FUNCTION');
    this.storage.get('otherNodes').then((val) => {
      if (val) {
        ApiNodeService.otherNodes = val;
      }
      this.storage.get('currentNode').then((val) => {
        if (val) {
          ApiNodeService.currentNode = val;
        }
        console.log('co vao day hay khong');
        ApiNodeService.setApiNode();
        ApiNodeService.checkCurrentNode();
      });
    });
  }

  getNetworkType() {
    return ApiNodeService.NETWORK_TYPE
  }
}
