import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  tags = []
  activeTag = []

  constructor(
    private storage: Storage,
  ) {
    this.getStorageTags()
  }

  async getStorageTags() {
    let tags = await this.storage.get('myTags')
    console.log(tags);

    if (tags === null) {
      tags = []
    }

    this.tags = tags
  }

  addTag(name) {
    let newElement = {
      name: name,
      active: false
    }

    let tag = null

    tag = this.tags.find(elem => elem.name === name)

    if ([undefined, null].includes(tag) === true) {
      this.tags.push(newElement)
    } else {
      console.error('The tag already exists')
    }

    this.storage.set('myTags', this.tags)
    console.log(this.storage.get('myTags'));

  }

  searchTagByName(name) {
    let tag = this.tags.find(elem => elem.name === name)
    return tag
  }

  activateTag(tagName) {
    this.tags.forEach(elem => {
      if (elem.name === tagName) {
        elem.active = !elem.active
      }
    })

    let tmpArr = this.tags.filter(elem => {
      if (elem.active === true) {
        return elem
      }
    })
    this.activeTag = tmpArr
  }

  turnOffTags() {
    this.tags.forEach(elem => {
      elem.active = false
    })
  }
}
