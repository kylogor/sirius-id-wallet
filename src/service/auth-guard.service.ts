import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private storage: Storage
  ) { }

  async canActivate(): Promise<boolean> {
    const privateKey = await this.storage.get('privateKey');
    const recovery = await this.storage.get('goToRecovery');
    console.log('PRIVATE KEY', privateKey);
    if (privateKey !== null || recovery === true) {
      return true;
    } else {
      this.router.navigate(['/home']);
      return false;
    }
  }
}
