import { Injectable } from '@angular/core';
import { NamespaceId, MosaicId, NamespaceHttp, MosaicHttp} from 'tsjs-xpx-chain-sdk';
import { ApiNodeService } from '../service/api-node.service';

@Injectable({
  providedIn: 'root'
})
export class GetCoinNameService {

  static coin_name:any;
  constructor() {}

  static async getCoinName(id: MosaicId | NamespaceId){
    console.log("getCoinName");
    console.log(id);
    if (id instanceof NamespaceId){
      console.log("here 1");
      let namespaceHttp = new NamespaceHttp(ApiNodeService.apiNode);
      return new Promise((resolve,reject) => {
        namespaceHttp.getNamespacesName([id]).subscribe(namespace => {
          console.log("2");
          console.log(namespace);
          if (namespace.length > 0){
            this.coin_name = namespace[0].name;
          }
          else this.coin_name = "Unknown";
          resolve(this.coin_name);
        }, error => {
          console.log(error);
        }, ()=>{
          console.log("done");
        }); 
      });
      
    }

    else {
      let mosaicHttp = new MosaicHttp(ApiNodeService.apiNode);
      return new Promise((resolve,reject) => {
        mosaicHttp.getMosaicsNames([id]).subscribe(mosaic => {
          if (mosaic[0].names.length > 0){
            this.coin_name = mosaic[0].names[0].name;
          }
          else this.coin_name = "Unknown";
          resolve(this.coin_name);
          }, error => {
            console.log(error);
          }, () => {
            console.log("done 2");
        });
      });
    }
  }
  
}
