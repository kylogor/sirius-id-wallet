import { TestBed } from '@angular/core/testing';

import { DeeplinkDataService } from './deeplink-data.service';

describe('DeeplinkDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeeplinkDataService = TestBed.get(DeeplinkDataService);
    expect(service).toBeTruthy();
  });
});
