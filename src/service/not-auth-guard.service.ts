import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
// import {ApiNodeService} from 'src/service/api-node.service';
@Injectable()
export class NotAuthGuardService implements CanActivate {

  auth = false;

  constructor(
    private router: Router,
    private storage: Storage,
    // private apiNode: ApiNodeService
  ) { }

  async canActivate(): Promise<boolean> {
    const privateKey = await this.storage.get('privateKey');
    console.log('PRIVATE KEY', privateKey);
    if (privateKey) {
      this.router.navigate(['/tabs']);
      return false;
    } else {
      return true;
    }
  }
}
