import { TestBed } from '@angular/core/testing';

import { GetCoinNameService } from './get-coin-name.service';
import { NamespaceId, MosaicId } from 'tsjs-xpx-chain-sdk';

describe('GetCoinNameService', () => {
  let service: GetCoinNameService;
  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.get(GetCoinNameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get coin name', async () => {
    const getByName = await GetCoinNameService.getCoinName(new NamespaceId('prx.xpx'))
    console.log(getByName)
    expect(getByName).toEqual('prx.xpx')

    
    const getById = await GetCoinNameService.getCoinName(new MosaicId([3825551831,331334936]))
    expect(getById).toEqual('prx.xpx')
    


  })


});
