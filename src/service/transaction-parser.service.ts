import { Injectable } from '@angular/core';
import {TransactionType, TransferTransaction, NetworkType, Address, NamespaceHttp, 
    AggregateTransactionCosignature,Deadline, PlainMessage,Mosaic,NamespaceId, MosaicId, UInt64, 
    AggregateTransaction, PublicAccount, ModifyMultisigAccountTransaction,
    MultisigCosignatoryModification, MultisigCosignatoryModificationType, AddressAliasTransaction, 
    AliasActionType, LinkAction, AccountLinkTransaction, RegisterNamespaceTransaction, 
    MosaicAliasTransaction, MosaicDefinitionTransaction, MosaicNonce, MosaicProperties, 
    MosaicSupplyChangeTransaction, MosaicSupplyType, AccountAddressRestrictionModificationTransaction, 
    RestrictionType, AccountRestrictionModification, RestrictionModificationType, 
    AccountOperationRestrictionModificationTransaction, 
    AccountMosaicRestrictionModificationTransaction, ModifyMetadataTransaction, 
    MetadataModification, MetadataModificationType, Id, ModifyContractTransaction, 
    SecretLockTransaction, HashType, SecretProofTransaction, ChainConfigTransaction, 
    ChainUpgradeTransaction,
    NamespaceType} from 'tsjs-xpx-chain-sdk';
  //import JSJoda = require('js-joda');
import {ChronoUnit, Duration} from 'js-joda';
import {ApiNodeService} from './api-node.service';
import { utils } from 'protractor';
@Injectable({
  providedIn: 'root'
})
export class TransactionParserService {

    constructor() { }

    static async parser(transaction:any){
        console.log("parser");
        switch (transaction.type){
            case TransactionType.TRANSFER:
                return await this.transfer(transaction);
            case TransactionType.AGGREGATE_COMPLETE:
                return await this.aggregate_completed(transaction);
            case TransactionType.AGGREGATE_BONDED:
                return await this.aggregate_bonded(transaction);
            case TransactionType.ADDRESS_ALIAS:
                return await this.address_alias(transaction);
            case TransactionType.LINK_ACCOUNT:
                return await this.link_account(transaction);
            case TransactionType.REGISTER_NAMESPACE:
                return await this.register_namespace(transaction);
            case TransactionType.MOSAIC_ALIAS:
                return await this.mosaic_alias(transaction);
            case TransactionType.MOSAIC_DEFINITION:
                return await this.mosaic_definition(transaction);
            case TransactionType.MOSAIC_SUPPLY_CHANGE:
                return await this.mosaic_supply_change(transaction);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_ADDRESS:
                return await this.modify_account_restriction_address(transaction);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_MOSAIC:
                return await this.modify_account_restriction_mosaic(transaction);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_OPERATION:
                return await this.modify_account_restriction_operation(transaction);
            case TransactionType.MODIFY_MOSAIC_METADATA:
                return await this.modify_mosaic_metadata(transaction);
            case TransactionType.MODIFY_ACCOUNT_METADATA:
                return await this.modify_account_metadata(transaction);
            case TransactionType.MODIFY_NAMESPACE_METADATA:
                return await this.modify_namespace_metadata(transaction);
            case TransactionType.MODIFY_CONTRACT:
                return await this.modify_contract(transaction);
            case TransactionType.SECRET_LOCK:
                return await this.secret_lock(transaction);
            case TransactionType.SECRET_PROOF:
                return await this.secret_proof(transaction);
            case TransactionType.CHAIN_CONFIGURE:
                return await this.chain_configure(transaction);
            case TransactionType.CHAIN_UPGRADE:
                return await this.chain_upgrade(transaction);
            default: return await this.transfer(transaction);
        }
    }

    static async transfer(transaction:any){
        console.log("transfer");
        const networkType = this.networkType(transaction.networkType);
        const recipient = Address.createFromRawAddress(transaction.recipient.address);
        const deadline = this.remakeDeadline(transaction.deadline);
        const message = PlainMessage.create(this.decodeHex(transaction.message.payload));
        const maxFee = new UInt64(transaction.maxFee);

        const mosaics = [];
        for (let i=0;i<transaction.mosaics.length;i++){
            console.log("loop");
            let id_tmp = transaction.mosaics[i].id;
            let resolve = await this.checkNamespaceId(id_tmp);
            let amount = new UInt64(transaction.mosaics[i].amount);
            if (resolve){
                console.log("namespaceId");
                let namespaceId = new NamespaceId(transaction.mosaics[i].id);
                var mosaic = new Mosaic(namespaceId,amount);
            }
            else {
                console.log("mosaicId");
                let mosaicId = new MosaicId(transaction.mosaics[i].id);
                var mosaic = new Mosaic(mosaicId,amount);
            }
            mosaics[i] = mosaic;
        }
        return TransferTransaction.create(deadline,recipient,mosaics,message,networkType,maxFee);
    }

    static async aggregate_completed(transaction:any){
        console.log("aggregate completed");
        console.log(transaction);
        const networkType = this.networkType(transaction.networkType);
        const maxFee = new UInt64(transaction.maxFee);
        const deadline = this.remakeDeadline(transaction.deadline);
        let innerTransaction = [];
        for (let i=0;i<transaction.transactions.length;i++){
            let tx = transaction.transactions[i].transaction;
            let pubAccount = PublicAccount.createFromPublicKey(tx.signer,networkType);
            let newTx;
            switch (tx.type){
                case TransactionType.TRANSFER:
                    newTx = await this.transfer(tx);
                    innerTransaction[i] = newTx.toAggregate(pubAccount);
                    break;
                default: 
                    console.log("default");
                    newTx = await this.transfer(tx);
                    innerTransaction[i] = newTx.toAggregate(pubAccount);
            }
        }
        let cosignatures = [];
        for (let i=0;i<transaction.cosignatures.length;i++){
            let cosignature = transaction.cosignatures[i];
            let signer = PublicAccount.createFromPublicKey(cosignature.signer.publicKey,networkType);
            cosignatures[i] = new AggregateTransactionCosignature(cosignature.signature,signer);
        }

        return AggregateTransaction.createComplete(deadline,innerTransaction,networkType,cosignatures,maxFee);
    } 


    static async aggregate_bonded(transaction:any){
        console.log("aggregate bonded");
        console.log(transaction);
        const networkType = this.networkType(transaction.networkType);
        const maxFee = new UInt64(transaction.maxFee);
        const deadline = this.remakeDeadline(transaction.deadline);

        let innerTransaction = [];
        for (let i=0;i<transaction.transactions.length;i++){
            let tx = transaction.transactions[i].transaction;
            let pubAccount = PublicAccount.createFromPublicKey(tx.signer,networkType);
            let newTx;
            switch (tx.type){
                case TransactionType.TRANSFER:
                    newTx = await this.transfer(tx);
                    innerTransaction[i] = newTx.toAggregate(pubAccount);
                    break;
                case TransactionType.MODIFY_MULTISIG_ACCOUNT:
                    newTx = await this.modify_multisig_account(tx);
                    innerTransaction[i] = newTx.toAggregate(pubAccount);
                    break;
                default: 
                    console.log("default");
                    newTx = await this.transfer(tx);
                    innerTransaction[i] = newTx.toAggregate(pubAccount);
            }
        }
        let cosignatures = [];
        for (let i=0;i<transaction.cosignatures.length;i++){
            let cosignature = transaction.cosignatures[i];
            let signer = PublicAccount.createFromPublicKey(cosignature.signer.publicKey,networkType);
            cosignatures[i] = new AggregateTransactionCosignature(cosignature.signature,signer);
        }
        return AggregateTransaction.createBonded(deadline,innerTransaction,networkType,cosignatures,maxFee);
    }

    static async modify_multisig_account(transaction:any){
        console.log("modify multisig account");
        console.log(transaction);
        const networkType = this.networkType(transaction.networkType);
        const maxFee = new UInt64(transaction.maxFee);
        const deadline = this.remakeDeadline(transaction.deadline);
        const minApproval = transaction.minApprovalDelta;
        const minRemoval = transaction.minRemovalDelta;

        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            let type = this.multisigCosignatoryModificationType(modification.type);
            let publicAccount = PublicAccount.createFromPublicKey(modification.cosignatoryPublicKey, networkType);
            modifications[i] = new MultisigCosignatoryModification(type,publicAccount);
        }
        return ModifyMultisigAccountTransaction.create(deadline,minApproval,minRemoval,modifications,networkType,maxFee);
    }

    static async address_alias(transaction:any){ 
        console.log("address_alias");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const aliasActionType = this.aliasActionType(transaction.aliasAction);
        const namespaceId = new NamespaceId(transaction.namespaceId.id);
        const address = Address.createFromRawAddress(transaction.address.address);
        return AddressAliasTransaction.create(deadline,aliasActionType,namespaceId,address,networkType,maxFee);
    }

    static async link_account(transaction:any){ 
        console.log("link_account");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const remoteAccountKey = transaction.remoteAccountKey;
        const linkAction = this.linkAction(transaction.action);
        return AccountLinkTransaction.create(deadline,remoteAccountKey,linkAction,networkType,maxFee);
    }

    static async register_namespace(transaction:any){ 
        console.log("register_namespace");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const namespaceName = transaction.namespaceName;
        if (transaction.namespaceType == NamespaceType.RootNamespace){
            const duration = new UInt64(transaction.duration);
            return RegisterNamespaceTransaction.createRootNamespace(deadline,namespaceName,duration,networkType,maxFee);
        }
        else {
            const parentNamespace = await this.getNamespaceName(transaction.parentId.id);;
            return RegisterNamespaceTransaction.createSubNamespace(deadline,namespaceName,parentNamespace,networkType,maxFee);
        }
        
    }

    static async mosaic_alias(transaction:any){
        console.log("mosaic_alias");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const aliasActionType = this.aliasActionType(transaction.aliasAction);
        const namespaceId = new NamespaceId(transaction.namespaceId.id);
        const mosaicId = new MosaicId(transaction.mosaicId.id);

        return MosaicAliasTransaction.create(deadline,aliasActionType,namespaceId,mosaicId,networkType,maxFee);
    }

    static async mosaic_definition(transaction:any){
        console.log("mosaic_definition");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const tmp = transaction.nonce.nonce;
        const nonce_number = Uint8Array.from([tmp["0"],tmp["1"],tmp["2"],tmp["3"]]);
        const nonce = new MosaicNonce(nonce_number);
        const mosaicId = new MosaicId(transaction.mosaicId.id);
        const params = this.mosaicProperties(transaction.properties);
        const mosaicProperties = MosaicProperties.create(params);

        return MosaicDefinitionTransaction.create(deadline,nonce,mosaicId,mosaicProperties,networkType,maxFee);
    }

    static async mosaic_supply_change(transaction:any){
        console.log("mosaic_supply_change");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const mosaicId = new MosaicId(transaction.mosaicId.id);
        const direction = this.mosaicSupplyType(transaction.direction);
        const delta = new UInt64(transaction.delta);
        return MosaicSupplyChangeTransaction.create(deadline,mosaicId,direction,delta,networkType,maxFee);
    }

    static async modify_account_restriction_address(transaction:any){
        console.log("modify account property address");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const restrictionType = this.restrictionType(transaction.restrictionType);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  AccountRestrictionModification.createForAddress(
                                    this.restrictionModificationType(modification.type),
                                    Address.createFromRawAddress(modification.value)
                                );
        }
        return AccountAddressRestrictionModificationTransaction.create(
            deadline,
            restrictionType,
            modifications,
            networkType,
            maxFee
        );
    }

    static async modify_account_restriction_mosaic(transaction:any){
        console.log("modify account property mosaic");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const restrictionType = this.restrictionType(transaction.restrictionType);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  AccountRestrictionModification.createForMosaic(
                                    this.restrictionModificationType(modification.type),
                                    new MosaicId(modification.value)
                                );
        }
        return AccountMosaicRestrictionModificationTransaction.create(
            deadline,
            restrictionType,
            modifications,
            networkType,
            maxFee
        );
    }

    static async modify_account_restriction_operation(transaction:any){
        console.log("modify account property entity");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const restrictionType = this.restrictionType(transaction.restrictionType);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  AccountRestrictionModification.createForOperation(
                                    this.restrictionModificationType(modification.type),
                                    modification.value
                                );
        }
        return AccountOperationRestrictionModificationTransaction.create(
            deadline,
            restrictionType,
            modifications,
            networkType,
            maxFee
        );
    }

    static async modify_mosaic_metadata(transaction:any){
        console.log("modify_mosaic_metadata");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const id = Id.fromHex(transaction.metadataId);
        const mosaicId = new MosaicId([id.lower,id.higher]);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  new MetadataModification(
                                    this.metadataModificationType(modification.type),
                                    modification.key,
                                    modification.value
                                );
        }
        return ModifyMetadataTransaction.createWithMosaicId(
            networkType,
            deadline,
            mosaicId,
            modifications,
            maxFee
        );
    }

    static async modify_account_metadata(transaction:any){
        console.log("modify_account_metadata");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const address = Address.createFromRawAddress(transaction.metadataId);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  new MetadataModification(
                                    this.metadataModificationType(modification.type),
                                    modification.key,
                                    modification.value
                                );
        }
        return ModifyMetadataTransaction.createWithAddress(
            networkType,
            deadline,
            address,
            modifications,
            maxFee
        );
    }

    static async modify_namespace_metadata(transaction:any){
        console.log("modify_namespace_metadata");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const id = Id.fromHex(transaction.metadataId);
        const namespaceId = new NamespaceId([id.lower,id.higher]);
        const modifications = [];
        for (let i=0;i<transaction.modifications.length;i++){
            let modification = transaction.modifications[i];
            modifications[i] =  new MetadataModification(
                                    this.metadataModificationType(modification.type),
                                    modification.key,
                                    modification.value
                                );
        }
        return ModifyMetadataTransaction.createWithNamespaceId(
            networkType,
            deadline,
            namespaceId,
            modifications,
            maxFee
        );
    }

    static async modify_contract(transaction:any){
        console.log("modify_contract");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const durationDelta = new UInt64(transaction.durationDelta);
        const hash = transaction.hash;
        const customers = [];
        for (let i=0;i<transaction.customers.length;i++){
            let customer = transaction.customers[i];
            customers[i] = new MultisigCosignatoryModification(
                this.multisigCosignatoryModificationType(customer.type),
                PublicAccount.createFromPublicKey(customer.cosignatoryPublicAccount.publicKey,networkType)
              );
        }
        const executors = [];
        for (let i=0;i<transaction.executors.length;i++){
            let executor = transaction.executors[i];
            executors[i] = new MultisigCosignatoryModification(
                this.multisigCosignatoryModificationType(executor.type),
                PublicAccount.createFromPublicKey(executor.cosignatoryPublicAccount.publicKey,networkType)
              );
        }
        const verifiers = [];
        for (let i=0;i<transaction.verifiers.length;i++){
            let verifier = transaction.verifiers[i];
            verifiers[i] = new MultisigCosignatoryModification(
                this.multisigCosignatoryModificationType(verifier.type),
                PublicAccount.createFromPublicKey(verifier.cosignatoryPublicAccount.publicKey,networkType)
              );
        }
        return ModifyContractTransaction.create(
            networkType,
            deadline,
            durationDelta,
            hash,
            customers,
            executors,
            verifiers,
            maxFee
        );
    }

    static async secret_lock(transaction:any){
        console.log("secret_lock");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        let resolve = await this.checkNamespaceId([transaction.mosaicId.lower,transaction.mosaicId.higher]);
        let amount = new UInt64(transaction.amount);
        if (resolve){
            console.log("namespaceId");
            let namespaceId = new NamespaceId([transaction.mosaicId.lower,transaction.mosaicId.higher]);
            var mosaic = new Mosaic(namespaceId,amount);
        }
        else {
            console.log("mosaicId");
            let mosaicId = new MosaicId([transaction.mosaicId.lower,transaction.mosaicId.higher]);
            var mosaic = new Mosaic(mosaicId,amount);
        }
        const secret = transaction.secret;
        const recipient = Address.createFromRawAddress(transaction.recipient.address);
        const duration = new UInt64(transaction.duration);
        const hashType = this.hashType(transaction.hashAlgorithm);
        return SecretLockTransaction.create(
            deadline,
            mosaic,
            duration,
            hashType,
            secret,
            recipient,
            networkType,
            maxFee
        );
    }

    static async secret_proof(transaction:any){
        console.log("secret_proof");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const secret = transaction.secret;
        const recipient = Address.createFromRawAddress(transaction.recipient.address);
        const proof = transaction.proof;
        const hashType = this.hashType(transaction.hashAlgorithm);
        return SecretProofTransaction.create(
            deadline,
            hashType,
            secret,
            recipient,
            proof,
            networkType,
            maxFee
        );
    }

    static async chain_configure(transaction:any){
        console.log("chain_configure");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const applyHeightDelta = new UInt64(transaction.applyHeightDelta);
        const networkConfig = transaction.networkConfig;
        const supportedEntityVersions = transaction.supportedEntityVersions;
        return ChainConfigTransaction.create(
            deadline,
            applyHeightDelta,
            networkConfig,
            supportedEntityVersions,
            networkType,
            maxFee
        );
    }

    static async chain_upgrade(transaction:any){
        console.log("chain_upgrade");
        const networkType = this.networkType(transaction.networkType);
        const deadline = this.remakeDeadline(transaction.deadline);
        const maxFee = new UInt64(transaction.maxFee);
        const upgradePeriod = new UInt64(transaction.upgradePeriod);
        const newBlockchainVersion = new UInt64(transaction.newBlockchainVersion);
        return ChainUpgradeTransaction.create(
            deadline,
            upgradePeriod,
            newBlockchainVersion,
            networkType,
            maxFee
        );
    }

    static networkType(networkType: number):NetworkType{
        switch (networkType){
            case NetworkType.MAIN_NET:
                return NetworkType.MAIN_NET;
            case NetworkType.TEST_NET:
                return NetworkType.TEST_NET;
            case NetworkType.MIJIN:
                return NetworkType.MIJIN;
            case NetworkType.MIJIN_TEST:
                    return NetworkType.MIJIN_TEST;
            case NetworkType.PRIVATE:
                return NetworkType.PRIVATE;
            case NetworkType.PRIVATE_TEST:
                return NetworkType.PRIVATE_TEST;
            default: return NetworkType.TEST_NET;
        }
    }
    static multisigCosignatoryModificationType(type:number):MultisigCosignatoryModificationType{
        switch (type){
            case MultisigCosignatoryModificationType.Add:
                return MultisigCosignatoryModificationType.Add;
            case MultisigCosignatoryModificationType.Remove:
                return MultisigCosignatoryModificationType.Remove;
            default: throw Error("Wrong type");
        }
    }
    static decodeHex(hex:string){
        let str = '';
        for (let i = 0; i < hex.length; i += 2) {
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        }
        return str;
    }


    static async checkNamespaceId(id: number[]){
        console.log("checkNamespaceId");
        console.log(id);
        let apiNode = ApiNodeService.apiNode;
        let namespaceId = new NamespaceId(id);
        console.log(namespaceId);
        console.log(apiNode);
        let namespaceHttp = new NamespaceHttp(apiNode);
        return new Promise(resolve => {
            namespaceHttp.getNamespace(namespaceId).subscribe(namespaceInfo => {
                resolve(true);
            }, error => {
                console.log(error);
                resolve(false);
            }, ()=>{
                console.log("done");
            }); 
        })
    }

    static async getNamespaceName(id: number[]):Promise<string>{
        console.log("getNamespaceName");
        let apiNode = ApiNodeService.apiNode;
        let namespaceId = new NamespaceId(id);
        console.log(namespaceId);
        console.log(apiNode);
        let namespaceHttp = new NamespaceHttp(apiNode);
        return new Promise(resolve => {
            namespaceHttp.getNamespacesName([namespaceId]).subscribe(namespaceName => {
                console.log(namespaceName[0].name);
                resolve(namespaceName[0].name);
            }, error => {
                console.log(error);
            }, ()=>{
                console.log("done");
            }); 
        })
    }

    static remakeDeadline(deadline){
        const timestampNemesisBlock = Deadline.timestampNemesisBlock;
        const exactly_deadline = (new UInt64(deadline)).compact() + timestampNemesisBlock * 1000;
        const now_deadline = (new Date()).getTime();
        const tmp = exactly_deadline - now_deadline;
        if (tmp < 0){
            throw Error('Expire deadline');
        }
        return Deadline.create(tmp,ChronoUnit.MILLIS);
    }

    static aliasActionType(type){
        switch (type){
            case AliasActionType.Link:
                return AliasActionType.Link;
            case AliasActionType.Unlink:
                return AliasActionType.Unlink;
            default: throw Error("type is wrong");;
        }
    }

    static linkAction(action){
        switch (action){
            case LinkAction.Link:
                return LinkAction.Link;
            case LinkAction.Unlink:
                return LinkAction.Unlink;
            default: throw Error("type is wrong");
        }
    }

    static mosaicSupplyType(type){
        switch (type){
            case MosaicSupplyType.Decrease:
                return MosaicSupplyType.Decrease;
            case MosaicSupplyType.Increase:
                return MosaicSupplyType.Increase;   
            default: throw Error("type is wrong");
        }
    }

    static restrictionType(type){
        switch (type){
            case RestrictionType.AllowAddress:
                return RestrictionType.AllowAddress;
            case RestrictionType.AllowMosaic:
                return RestrictionType.AllowMosaic;  
            case RestrictionType.AllowTransaction:
                return RestrictionType.AllowTransaction;
            case RestrictionType.BlockAddress:
                return RestrictionType.BlockAddress;  
            case RestrictionType.BlockMosaic:
                return RestrictionType.BlockMosaic;
            case RestrictionType.BlockTransaction:
                return RestrictionType.BlockTransaction;  
            case RestrictionType.Sentinel:
                return RestrictionType.Sentinel;   
            default: throw Error("type is wrong");
        }
    }
    static metadataModificationType(type){
        switch (type){
            case MetadataModificationType.ADD:
                return MetadataModificationType.ADD;
            case MetadataModificationType.REMOVE:
                return MetadataModificationType.REMOVE;   
            default: throw Error("type is wrong");
        }
    }
    static restrictionModificationType(type){
        switch (type){
            case RestrictionModificationType.Add:
                return RestrictionModificationType.Add;
            case RestrictionModificationType.Remove:
                return RestrictionModificationType.Remove;   
            default: throw Error("type is wrong");
        }
    }
    static hashType(type){
        switch (type){
            case HashType.Op_Hash_160:
                return HashType.Op_Hash_160;
            case HashType.Op_Hash_256:
                return HashType.Op_Hash_256;
            case HashType.Op_Keccak_256:
                return HashType.Op_Keccak_256;
            case HashType.Op_Sha3_256:
                return HashType.Op_Sha3_256;     
            default: throw Error("type is wrong");
        }
    }
    static mosaicProperties(properties){
        const property0 = new UInt64(properties[0].value).compact();
        let supplyMutable:boolean;
        let transferable:boolean;
        if (property0 == 0){
            supplyMutable = false;
            transferable = false;
        }
        else if (property0 == 1){
            supplyMutable = true;
            transferable = false;
        }
        else if (property0 == 2){
            supplyMutable = false;
            transferable = true;
        }
        else {
            supplyMutable = true;
            transferable = true;
        }
        return {
            supplyMutable: supplyMutable,
            transferable: transferable,
            divisibility: new UInt64(properties[1].value).compact(),
            duration: new UInt64(properties[2].value)
        }
    }
}
