import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiNodeService } from 'src/service/api-node.service';
import { AccountHttp, Address, TransferTransaction, Deadline, UInt64, PlainMessage } from 'tsjs-xpx-chain-sdk';
import { AnnounceTransaction } from 'siriusid-sdk';
@Injectable({
  providedIn: 'root'
})
export class StoredAccountService {

  static privateKey: string;
  static publicKey: string;
  static address: string;
  static storage: Storage;
  isOnBlockChain: boolean;
  isSending = false;

  constructor(private storage: Storage) {
    StoredAccountService.storage = this.storage;
    console.log('\n\n ------------ INIT CONSTRUCTIR STORED ACCOUNT ----------------\n\n');
    this.storage.get('privateKey').then((val) => {
      console.log('MY PRIVATE KEY', val);
      StoredAccountService.privateKey = val;
    });

    this.storage.get('publicKey').then((val) => {
      console.log('MY PUBLIC KEY', val);
      StoredAccountService.publicKey = val;
    });

    this.storage.get('address').then((val) => {
      console.log('MY ADDRESS', val);
      if (val) {
        StoredAccountService.address = val;
        this.checkAccount();
      }
    });
  }

  static setPrivateKey(privateKey: string) {
    this.privateKey = privateKey;
    this.storage.set('privateKey',privateKey);
  }

  static getPrivateKey() {
    return this.privateKey;
  }

  static setPublicKey(publicKey: string) {
    this.publicKey = publicKey;
    this.storage.set('publicKey',publicKey);
  }

  static getPublicKey() {
    return this.publicKey;
  }

  static setAddress(address: string) {
    this.address = address;
    this.storage.set('address',address);
  }

  static getAddress() {
    return this.address;
  }

  /**
   *
   *
   * @memberof StoredAccountService
   */
  async initAccount() {
    console.log('_______ INIT ACCOUNT IN STORED SERVICE _________');
    if (StoredAccountService.address) {
      this.isSending = true;
      const address = Address.createFromRawAddress(StoredAccountService.address);
      if (ApiNodeService.apiNode) {
        const live = await ApiNodeService.checkNodeLive(ApiNodeService.apiNode);
        if (live) {
          const tx = TransferTransaction.create(
            Deadline.create(),
            address,
            [],
            PlainMessage.create('Init account'),
            ApiNodeService.NETWORK_TYPE,
            new UInt64([0, 0])
          );

          AnnounceTransaction.announce(tx,
            StoredAccountService.privateKey,
            ApiNodeService.apiNode,
            ApiNodeService.NETWORK_TYPE
          ).then((result) => {
            console.log('init succeeded');
            this.isSending = false;
            this.isOnBlockChain = result as boolean;
          }).catch((error) => {
            this.isSending = false;
            console.log(error);
          });
        } else {
          this.isSending = false;
          console.log('node die or no internet connection');
        }
      } else {
        this.isSending = false;
        console.log('api node not ready');
      }
    }
  }

  /**
   *
   *
   * @memberof StoredAccountService
   */
  async checkAccount() {
    console.log('---------- Check account ----------');
    if (StoredAccountService.address) {
      const address = Address.createFromRawAddress(StoredAccountService.address);
      if (ApiNodeService.apiNode) {
        console.log('Check node live.....');
        const live = await ApiNodeService.checkNodeLive(ApiNodeService.apiNode);
        if (live) {
          console.log('SEARCH ACCOUNT INFO');
          const accountHttp = new AccountHttp(ApiNodeService.apiNode);
          accountHttp.getAccountInfo(address).subscribe((result) => {
            console.log('GET ACCOUNT INFO ----> ', result);
            this.isOnBlockChain = true;
          }, error => {
            console.log('ERROR IN GET ACCOUNT INFO --->', error);
            this.isOnBlockChain = false;
          }, () => {
            console.log('Done! Finished...');
          });
        } else { console.log('Node dies or there is no internet connection'); }
      } else { console.log('Api node not ready'); }
    }
  }
}
