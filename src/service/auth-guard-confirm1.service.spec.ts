import { TestBed } from '@angular/core/testing';

import { AuthGuardConfirm1Service } from './auth-guard-confirm1.service';
import { Router } from '@angular/router';
class MockRouter{
  navigateByUrl(url: string) { return url; }
}
describe('AuthGuardConfirm1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Router, useClass: MockRouter },
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardConfirm1Service = TestBed.get(AuthGuardConfirm1Service);
    expect(service).toBeTruthy();
  });
});
