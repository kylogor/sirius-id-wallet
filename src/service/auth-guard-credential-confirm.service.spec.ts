import { TestBed } from '@angular/core/testing';

import { AuthGuardCredentialConfirmService } from './auth-guard-credential-confirm.service';

describe('AuthGuardCredentialConfirmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardCredentialConfirmService = TestBed.get(AuthGuardCredentialConfirmService);
    expect(service).toBeTruthy();
  });
});
