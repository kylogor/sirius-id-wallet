const fs = require('fs');

var pjson = require('./package.json');

console.log(pjson.version);

function updateVersion() {
    fs.readFile("config.xml", 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        const start = data.indexOf(' version="');
        const end = data.indexOf('xmlns');
        const str = data.slice(start+10,end-2);
        console.log(str);

        const oldStr = '<widget id="io.proximax.siriusID" version="' + str + '" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">'
        const newStr = '<widget id="io.proximax.siriusID" version="' + pjson.version + '" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">'
        const result = data.replace(oldStr, newStr);

        fs.writeFile("config.xml", result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

updateVersion();

