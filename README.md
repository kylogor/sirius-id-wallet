# Official SiriusID Identity Wallet

This is the official source code repository of SiriusID Identity wallet. 

## Building / Installation

### Step 1: Clone the repository and make sure the builds are successful.

```
git clone https://gitlab.com/proximax-enterprise/siriusid/sirius-id-wallet.git
cd sirius-id-wallet
npm install
```

### Step 2: Install the native-run command line interpreter library. 
```
npm i -g native-run
```

### Step 3: Make sure you have the emulators on your local device.

For Android, install the [ADT](https://developer.android.com/studio). The ADT will allow the developer to install Android OS emulators on your local development machine. 


For iOS, install [XCode](https://developer.apple.com/xcode/). Similar to Android, the XCode will allow the developer to run iOS emulator on the local development machine.

### Step 4: Build, Compile and Run

Run the build again using `npm install`

Build Android 
```sh
cd sirius-id-wallet
npm install
ionic cordova build android --prod
```

Build iOS 
```sh
cd sirius-id-wallet
npm install
ionic cordova build ios --prod
```

Run the following command to deploy the application on your emulator.

```
ionic cordova run
```

