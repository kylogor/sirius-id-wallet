# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.18](https://gitlab.com/proximax-enterprise/siriusid/sirius-id-wallet/compare/v0.1.17...v0.1.18) (2020-04-03)

### [0.1.18](https://gitlab.com/proximax-enterprise/siriusid/sirius-id-wallet/compare/v0.1.17...v0.1.18) (2020-04-03)

### [0.1.17](https://gitlab.com/proximax-enterprise/siriusid/sirius-id-wallet/compare/v0.1.16...v0.1.17) (2020-03-28)

### 0.1.16 (2020-03-23)
